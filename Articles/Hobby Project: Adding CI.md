This next session was to get some tests up and running. As it is 2020 this means adding a CI system [(which is not continuous integration)](https://dev.to/jessekphillips/git-branch-strategy-part-1-3m80). 

First thing I did was to refactor the query method so I can verify parsing out the data I am interested in. I don't really view this as a valuable test, but I now have a helper method to hide the deserialization in. 

I created [merge request 1](https://gitlab.com/jessephillips/devarticlator/merge_requests/1) which includes steps for unittesting and build. Which has already paid dividends as I forgot files and parts of files. This allows me to work through desired changes.

Next I expect I'll be building out how I want to write out the articles and their Metadata. 