---
title: What does your Test Cover? 
published: true
description: 
tags: discussion 
---

My last post asked to include readability when evaluating code structure and design along with many of the other objectives like testing code.

{% link https://dev.to/jessekphillips/software-design-through-tdd-4mi7 %} 

I don't write production code on a daily basis, I'm a software test engineer who has to assure the quality of deliverables to the clients. I don't do this through test plans or reams of regression tests. I utilize an understanding of the system, support from software tools, history of problems, and asking questions. This leaves me with an ability to identify swathes of testing I should not do and I keep reducing that work to meet risk vs cost efforts.

To summarize, there is so much to test and you want to find what not to test. Does anyone do analysis of what types of test coverage they have and how effective it is at finding code issues vs needing updates due to expected changes? I expect that tests which cover logic over layout provides the most benefit.

What are testing types you find falsely claiming failures needing great maintenance and not carrying their weight? 