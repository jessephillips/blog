---
title: Partnered Code Reviews
published: true 
description: 
tags: git, pullrequest
series: Git Communication
---

Are you familiar with code reviews? Do you do the review in person or through a change request tool (merge/pull)? What benefit do you get having someone stand next to as you talk through the changes you made?

Git conventional commits facilitates a partnered code review without the partner. When working to decide how to organize your changes, consider these questions/considerations 

* What am I changing, Why
* Will this help me tell someone what I just did
* Does this include all the needed changes such that the review can confirm all needed components (especially those which talk to each other) have been correctly modified.
* Would this be useful if reverted
* Do I need to group many commits together for reverting
* Should this commit go upstream

The first question is what a code review causes you to ask so it can be explained to your partner. 

# Reality

The actual results of your Commits will very, partially from disopline, accountability, and laziness. I believe there can be good reasons for making exceptions because dev work can be messy, but having a goal to get close will be beneficial because you'll first have to ask yourself, why is this an exception and be happy with that answer. 