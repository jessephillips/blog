I [commented on unpopular opinions](https://dev.to/jessekphillips/comment/mcd4) and wanted to expand on this in a post. Let me break this up into two parts, the new and the old. 

# The New

In many ways software development has regressed. Not in quality or functionality. No it has regressed in longevity. (please stop listening to me, I have not been programming that long).

What I mean is we are in the current Era of exploration. There is much change occurring and a lot of need to advance. The value of progress is greater than stability and consistency. 

Have you ever had to raise a kid? Did you notice that they don't know the same things as you? It is like all of the progress of humanity has just been wiped clean and all is lost in the world. 

Well those wascally wabbits start to learn things you don't know, they start doing things you find regressive.

# The Old

The past sits there, it is a foundation to make changes. It is a pestering, inconsistent, counter intuitive, piece of garbage that had a place in the progress of humanity. But it can't be shaken no matter how much garbage is put on top. But HTTP has still given us wonders.

The foundation has provided a new platform of exploration and mistakes. Most of the time you can rely on it existing, and that makes it hard to change.

# What Can I Do

One of the key powers of the past is its existence and it is consistently available. But new things aren't. We don't know what new thing do we all agree to use. So many choices, so many opinions and needs.

There was less competition back in the day. Literally it is why we still have [more](https://superuser.com/questions/310137/less-is-more-is-more-less-im-so-confused). If we won't have eggs until we have chickens, maybe we should all start being chickens.

The challenge I see is, even if we have better tools out there, our expert knowledge, our ability to help new recruits is limited if they choose to go down a simpler path. 

I am going to look into 

* Using Fish as my shell (already looks like I can't do this with Git for windows)
* check out an alternative for Git command line

I don't really see the git one working out for me. I think auto stash is a horrible idea, but that appears to be major selling point. But git itself has seen simplifying commands, I need to learn them. 

I won't be leaving vim. 

Any suggestions would be appreciated. In this case I am looking for CLI tools because of the specific benefits of having a CLI to help people.

{% link https://dev.to/jessekphillips/wip-cli-or-gui-what-do-i-use-2dhl %} 