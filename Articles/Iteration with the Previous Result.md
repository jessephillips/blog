---
title: Iteration with the Previous Result
published: true
description: 
tags: dlang, algorithm, ranges
---

I am working with zip archives and wanted to create an archive for my unittesting. This creation does no touch the file system so I need to specify the structure. 

To facilitate directory structure I'll need to archive the directory tree. Actually for my purposes it looks like I don't but that isn't the point. 

I put together this range which allow for create each stage of directory, [With Previous](https://run.dlang.io/gist/JesseKPhillips/9fe592976261db05c7a83d4d631f74ef?compiler=dmd&args=-unittest%20-MAIN).

```dlang
    import std.path;
    import std.algorithm;
    static assert(r"foo/bar/foobar".pathSplitter
         .withPrevious!buildPath
         .equal([
                buildPath("foo"),
                buildPath("foo","bar"),
                buildPath("foo","bar","foobar")]));

```

I specify a path as a string and use pathSplitter to provide each part of the path. 

I feed this to my withPrevious range and utilizes buildPath to create a OS specific path from components.

The result is equal to each part built on the previous. I don't think I will utilize this for my unittests, but I do like what this provides and is not specific to paths or strings.