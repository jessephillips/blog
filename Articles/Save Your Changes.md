---
title: Save Your Changes
published: true
description: Commit everything to the database. 
tags: git
---

Git likes change, it wants to keep your changes safe. When changing branches git will keep your working tree changes during the switch, but  if it needs to modify one of those files it will not switch branches. 

Git wants to keep your changes safe so it emphasizes that you commit, stash (you can do that too). It gives tools to make your Commits clear. Interactive rebase is your best friend.

Because git knows the file tree well, you never need to clone a clean folder. There is almost always a command to cleanup a mess you made. When in doubt, commit and switch branch. It fact git is so good at this if you add a completely unrelated repo as a remote you can work out of a single folder. Not recommended.

Do not use guis, they just pop-up dialogs when they will destroy changes without warning. In the command line you run the safe operation then rerun with --force when verified the destruction was desired. 