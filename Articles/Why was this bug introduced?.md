---
title: Why was this bug introduced? 
published: true
description: 
tags: git 
series: Git Communication
---

In the previous posts I established what can be communicated when changing code. Now we come to weeks or years later and have a question, Why was this bug introduced?

Generally this happens by the PM or client when the bug was found, there is also a similar question which places the blame squarely on the tester, Why didn't you catch this? But let's talk about why as a developer you would ask yourself the first question.

This question would be asked once you find where the bug is. Since that is 80-90%of the battle you've got only a little bit to read the code and adjust to avoid the situation you're in.

Now did you read the comment on that line, the one explaining why it was put there? What if your change just reverted a commit for a different bug? With good communications through commits every line of code has the answer to this question and it is pulled up using git blame.

Git blame annotates every line of code with the commit which added/modified it. You can jump back through history to see how the file/line has changed over time and most importantly view the commit and message of that change.

Fore help with the other 80% there is git bisect.

https://git-scm.com/book/en/v2/Git-Tools-Debugging-with-Git

{% link https://dev.to/eamodio/its-not-just-about-theblame-32a2 %} 