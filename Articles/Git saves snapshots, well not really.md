---
title: Git saves snapshots, well not really 
published: true
description: 
tags: git
---

I've seen a couple articles to try and explain git as a system of snapshots rather than changes. I'm really not sure how this explanation is supposed to help when working with git. Especially since git doesn't always save entire blobs, using an assortment of partial file diffs to save space.

Git is also really good at moving changes not just blobs from one place to another.

So maybe someone can explain why I would want to use snapshots to explain git. 