---
title: Defending Submoduals for Git
published: true
description: I like submoduals, I've learned to take it back a notch. 
tags: git
---

Submoduals tend to get dismissed as a bad solution and alternatives would be package managers (nuget) or even git subtrees.

Let me start with the ideal. For a given program all the source for every part is saved in a single repository and neatly organized. In this ideal world the is no other program, code sharing is only within the different components of this program. (I don't want you to image what kind of code base would be a browser and Unreal engine... This world only cares about the standard app you write today)

Why is this ideal? Because any bug fix or feature is fully controlled from a single location. But reality hits and we have many reasons to separate our code. 

* different languages needing different build pipelines 
* build time
* share with other applications 
* enforce separation of concerns

I want to advocate for submoduals over package managers for your internal and developed components. If you are in control of the source and it is not used by others you don't need to coordinate a release, set up internal package repos, expand your storage to permanently retain official releases. Especially if these components can't be thoroughly tested separate from the parts they plug into.

With submoduals a project can share parts of code, the code can change and be utilized as each project needs it to. Yes managing the changes and propagating fixes to all requires action. Each alternative has the same challenge it just uses different mechanics.

I do wish there was better tool support for submoduals, git has been getting the appropriate attention though. 