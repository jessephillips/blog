# Preamble 

So what I realized is this argument is basically the same as this post. 

{% link https://dev.to/jessekphillips/the-uncanny-valley-3h6h %} 

# YAML 

"YAML is a human friendly data serialization standard for all programming languages."

It has been around since 2001 and recently it is grown in popularity. I'd like to curb this growth.

I don't have a full history of data serialization, but here are some points. 

Binary serialization - Some early serialization was a dump of memory to disk. Today we hear cross-platform we think OS, hardware stores data differently (the big one being endian). As such this was not a good strategy for sharing.

Human Readable - XML was a big deal. It could be read and modified by plain text editors and it was based on something familiar ~~SGML~~ HTML. It was expressive enough that you could represent anything with it, and we did. 

Schema Free - XML was massive and emphasized using schemas even though most parsers didn't require it. This was a pain for the fast moving web which didn't want to be brought down by the shackles of standards. JSON didn't need a schema and the web language could already parse it. (some people are still trying to get schemas into JSON. 

Human Friendly - Sure HTTP, XML, and JSON can be read by humans, but they aren't very friendly to us mortals. JSON got rid of the tags, but left much in the way with weird punctuation. Enter ~~Markdown~~ YAML a language which looks like a story written by Shakespeare but machine readable too. 

Problem is, Shakespeare wrote in old English. The things he has written look similar but can have a different meaning and thus the time period need to be studied to actually know the meaning. 

YAML wants you to feel at home, but if you're using it as a poor man's batch script (looking at you gitlab) then you're going to use things YAML assigns a special meaning to. 

You're going to tell me that I should have the batch script in a file and run that instead. Well that is what pushed me over because that is what I was trying to do. Powershell uses & for file execution and YAML uses it for anchors. So now I need to quote the entire command to make it valid.

JSON requires escape inside strings, but this is well defined as you know where it expects structure. YAML trying to be friendly requires extra structure when there is ambiguity.

On top of that being hard for humans & it being valid writing, YAML is hard to parse. Whitespace has meaning, except if you are writing a list then you have a couple of choices. And while references are supported, how well does that work for you when you need to support JSON reference conversation (looking at you OpenAPI)? 