I've come to realize that I need to emphasize the importance of refactoring. This should not be a surprise, TDD and other development techniques describe it as fundamental to the process. However not much is written from the perspective of a tester.

# Do Not Refactor

The general perspective that a tester might have is to avoid making additional changes. The reason for this is that the changes add risk that things won't work. 

It could mean a full system testing effort because, who knows what downstream effects that would have?

# You Better Refactor

Refactoring is the process of making code changes such that the end result would be the same behavior. This would even include producing the same bugs.

This creates the ultimate in sales pitch. "I see you're remodeling your kitchen, trying to open up more space?" 

"No" 

"Oh, are you looking to modernize the style?"

"No, still the same styles and colors."

"Well,then why are you remodeling, was it not up to code." 

"When we're done remodeling it still won't be up to code. But it will be easier to remodel the next time."

In software development this is very much an important step to take. Unlike a house, software development is the process in which you change things. If your house was undergoing constant renovation you'd be looking at making it easier and cheaper to perform the changes you want as well. 

# Refactoring Embodies Quality

When doing a refactor you are targeting a portion of the program which does not clearly state the intention or purpose of the code. Some use this as an excuse to inject tests, others want to change allow for a new API that provides more options, and sometimes it can be an improvement to performance (the one place you might actually be asked to refactor from the client).

# Refactoring has Down Stream Effects

Aside from the challenge of keeping the same behavior (even when a new behavior could be so much better), if you have a collaborative environment the refactoring could create pain for those working in the same area. 

This is where short lived branches and continuous integration come into play. Large refactoring will either fail or be a huge pain. 

But even with a smaller refactor, having clarity on what the supported APIs are help to define limits of a refactor or identify the need for coordination.

If there are layers of source control management, frequent integration of code refactors before those other layers could integrate is as bad as a large refactoring.

Please do refactor, work with QA to make it easier, and integrate frequently so others can refactor. 