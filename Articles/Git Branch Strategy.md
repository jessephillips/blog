Branching strategies are a popular topic. We have git flow, Github flow, flow git, trunk dev, dev trunk, truck flow dev git. OK most of those are made up.

Why are their so many branching strategies? Why are some so much more complex than others?

To answer this question let us talk about two important aspects of a good branching strategy. 

* Communication 
* Continuous Integration 

# Communication 

First off
{% link https://dev.to/jessekphillips/git-is-a-communication-tool--2j9k %} 

But what communication is a branch doing? What you want the branch to tell you... I'm going to state what a branch means, ready? 

Here lies a supported or maintained code base. 

## Supported Code

Now we need to understand how someone supports a branch. Support refers to applying changes to the code. 

## Maintained Code

Is maintenence different from support? 
Maintained means development is actively occurring. 

## Example of Improper Support

I think a negative example is best for understanding the distinction. 

Say a beta build is created off a branch. Development continues on new features and bug fixes. A report of a bug on the beta build which needs fixed. The team fixes the bug and creates a new beta release or RC. 

While the bug was addressed, the code base changed, the original code from the beta was not receiving the support because the code base was being maintained. 

## That was Pointless 

Clearly if we are getting the fix out it doesn't matter it came with other improvements. 

This distinction has more value when considering risk mitigation. If the objective is to get the original beta out into production. These new features and even bug fixes could delay that objective by including/introducing bugs themselves. 

### Beta Should Not Go to Production 

Clearly the build was marked as a beta release. As we were still building out features for the next release it was not ready for the release support. 

As long as you have a clear understanding what is communicated, this is correct. However I may want to talk with you about 'potentially releasable builds.' 

# Continuous Integration

The next concept which is so overloaded to tackle is continuous integration.

“continuous integration (CI) is the practice of merging all developers' working copies to a shared mainline several times a day." -- [Wikipedia](https://en.m.wikipedia.org/wiki/Continuous_integration)

Reading further you'll notice unittesting and even build servers are mentioned to support this workflow. These are not requirements. Instead we have two critical parts.

* Multiple Developers 
* A shared location of collaboration 

This means if you have a mainline, build server executing tests, and you are not continuously taking changes from other developers, you are not doing CI.

## CI Branching

Your branch strategy should take into account if you are doing continuous integration. 

This means crafting branches which are taking changes but don't continuously integrate the rest of development would be a model without CI.

### Release Branches aren't Supposed to Integrate 

Yes, but work done on them should.

Be sure to understand this interrelationships of CI, maintaining, supporting, branching. 