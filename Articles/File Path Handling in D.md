Working with file paths is very annoying, especially on Windows where it supports both separators most of the time.

Aside from deciding which slash to use there are a couple of areas to consider. 

* Home directory 
* Relative vs absolute

D provides quick work of these. 


[std.path](https://dlang.org/phobos/std_path.html) 

```dlang
import std.path;
buildNormalizedPath("foo/bar/../peas"
            .absolutePath.expandTilde);
```

However trying to build a path by adding slash separators is not the best choice for building a path from variables. 

```dlang 
auto a = "foo";
auto b = "bar";
    
assert([a, b].joiner("/").equal(a.buildPath(b)));
```

And further work can be done on extensions. 

```dlang
assert("file.ext".extension == ".ext");
assert("file.ext".setExtension("gif") == "file.gif");
assert("file.ext".stripExtension == "file");
```