After writing a couple articles which directly compared Python with D, I realized I should bring additional languages into the mix.

As such I will try my hand at a new series (with no specific direction). [Rosetta Code] (http://www.rosettacode.org/wiki/Rosetta_Code) provides similar language comparison, but I think I can dive into some semantic differences. While I won't have specific language to make a comparison for, D will definitely be involved, Python, C#, Lua, Java, Javascript are definite candidates. 

I'd like to focus on the features with interesting differences and the languages which present them, but sometimes it might just be synthetic. 

-----

For reference, this was my reply to my comparison to Python. 

{% link 
https://dev.to/jessekphillips/what-is-with-all-the-python-hate-5c9j %} 