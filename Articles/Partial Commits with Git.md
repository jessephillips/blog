---
title: Partial Commits with Git
published: True
description: You can choose what to commit. 
tags: git
---

I had some success bringing better commits to our repo.

I happened across a post like this one.

{% link https://dev.to/sublimegeek/semantic-commit-messages-with-emojis-3p8h %}

See I don't really like emoji and people can go overboard, but others do like it so when you give them a dashboard like [gitmoji](https://gitmoji.carloscuesta.me) and tell them to add emoji to their commit message, you start to talk about all the emoji in the message, your commit should only need one...

Which brings me to the partial commit. See git has a staging area which is completely separate from the working tree. Many know you can stage files and leave others uncommitted.

With the right tool (git gui) you can stage just one line. When I first brought up splitting the commit, the reaction is fear because their is so much pain in the stash and edit to break them up. Why would they expect to be able to control lines when UIs don't make the feature obvious and it isn't mentioned in git training. 