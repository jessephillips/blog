---
title: My old git processes
published: true
description: How to rebase like a champ. 
tags: git
---

I've transitioned to using rebase interactive but I think what I use to do may help others become more comfortable with moving changes around. The goal of these are to organize commits because they are on a branch with other changes or I want to combine commits. 

First off, I make sure I have gitk open. This tool is nice because using F5 to refresh maintains the previous known tree state.

Create a new branch at the commit you want to place your commits. Now cherry pick (right click the commit in gitk) the desired change. 

If you want to combine commit with the last one then use

    git reset --soft HEAD~2
    git commit -m...

I am able to cherry pick my commits in any order and I can make the commit history smaller by combining related commits. 

Now that you have gone through this, read through git rebase interactive.

{% link https://dev.to/koffeinfrei/the-git-fixup-workflow-386d?utm_source=additional_box&utm_medium=internal&utm_campaign=regular&booster_org= %} 