When dealing with testing software, I've determined that it is important to communicate things aren't going to be tested. We want to have good test coverage to meet the need of having confidence in our releases, but with limited man power and rigid deadlines, choices about where to focus testing need to be made. It's not like this isn't understood or that such statements are unknown, but I think the implications of such a situation are not understood. 

I want to talk about something which I feel is well described as, "the eyeball effects" because it relates to how many and what eyes are on a release. This really has two impacts related to testing.

* It creates pressure for QA to deliver a functioning and bug free solution
* It also provides an opportunity to leverage distribution of work.

This focus usually comes in relation to a new feature or a high profile bug. This extra focus isn't a huge issue but when combined with only QA can 'officially' test we have many duplication in testing. 

Part of this can be addressed by having QA document the effort which took place. But this becomes a challenge since a QA member feels they are providing the approval but were not the one in control of the testing.

Personally I find too stringent requirements on documenting testing done can prevent good collaboration strategies. I don't now how many times I've been over a devs shoulder as he walks me through some testing he is doing, and we discuss edge cases.

I realize this throws out the awesome regressions for future testing. Well I don't believe a manual regression is a good plan for future proofing. 