Merry Christmas, please enjoy this fun walk into a DSL of an old computer game. 

ZZT was an early video game by Epic's founder Tim Sweeney. It was a two dimension ascii based DOS game.

The game comes with a level editor which allows you to build your own games. To build a room you place objective on the screen. Many of the objects have pre defined behavior and can not be edited, the interesting ones are those that can.

Let's start with some basics. Since these objects live in a game world the language focus on how to operate in that world. 

```zzt
\north
```

This moves the object upward on the screen. The four cardinal directions are available for navigating the screen. The object can also interact with players properties 

```zzt
#take health 5
#give ammo 10
This message is shown to the player
```

Code flow is executed from top to bottom and includes labels and goto. 

```zzt
:loop
Hello world 
/idle/i/i/i/i
#goto loop
```

Now that we have a way to navigate to specify code let's name our object, done as the first line of the object code. 

```zzt
@niceguy
#end
:punch
Ouch, that hurt
#end
:greet
I love shooting 
#give ammo 3
#end
:touch
#give torch 7
#goto punch
```

This provides a niceguy with different code paths (punch, touch, greet). Let's build another object.

```zzt
@badguy
Where are you? You will be sorry when I find you. 
:loop
/seek
#if contact then caught
#goto loop
:caught
#send niceguy:punch
#take health 20
#take ammo 7
#take tourch 6
/idle
#end
```

This character will move toward the player and if he comes in contact, he punches niceguy. That is what `send` does is causes execution to run on the other object.

Often in object oriented programming calling a method is referred to as sending the object a message. ZZT OOP, method calls are sending the object a message.

Update;

Hene is on of my games : https://museumofzzt.com/file/l/where.zip