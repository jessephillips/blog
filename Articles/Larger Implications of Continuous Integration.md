---
title: Larger Implications of Continuous Integration 
published: true
description: We've lost the mean of Continuous Integration.
tags: git, ci
---

I'm noticing a pattern and it has to do with the overloaded process of continuous integration. This has become a reference to executing tests on every commit. I'm not saying this isn't important. 

I am seeing articles about long lived branches and trunk based development. These articles are all about continuous integration, but they can't reference this age old solution because it is no longer about integration.

Now that we understand trunk based development is just continuous integration for the git era of version control let me talk to you about micro services and sharing code through package managers. 

The explanation behind them is that they help decoupling and independent development. But independent from what? These usually are consumed by software which may wish to work with multiple services together.

Are you the consumer of these service or providing them to others? It does not really matter your just building one part of an integrated system, as a provider you will still want continuous integration with your changes and as a consumer you don't want to get left behind on old deprecated APIs.

No matter how you organize your application don't forget the lessons which lead to continuous integration. 