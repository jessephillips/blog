---
title: New Here, think I'm getting around
published: true
description: What do I need to know?
tags: meta
---

I'm coming up on my one week anniversary. I posting this to include some thoughts to the admins, also to see if I can get some clarity, and point out some things I found along the way. First off, why did I sign up after all "how many times do you sign up after you've landed on a random website coming from Google" - https://dev.to/ben/comment/7boc

I arrived through some articles that showed up on my Google Assistant news feed. The article was of interest to me but I found that it perpetuated a concept which I thought was only prevalent in my circle. I wanted answers because in my circle I am getting my information third hand and that makes it hard to get at the bottom of it. So I signed up to ask the question in a comment. Long story short, I still don't understand but it drove me to continue pushing my thoughts out. I like a personal blog, but without followers you don't get feedback.

New users, posting help is right at the top of your new post. It is sad that it takes over what you're working on and need to scroll around but it is there.

I'm posting this to tag #meta, I think that is acceptable, I didn't see any guidelines on how I may want to choose tags. #help exists, maybe that should be there too?

The Faq states cross posting to your blog is OK. What are the expectations of this? I think a link isn't sufficient, should I summarize first or use the first paragraph. I copied the entire content when I did it. Am I supposed to use the canonical_url for this? 

Then we have the front page, the devto editors select these. There is a list selection to see newest posts. But I'd be interested in how  my feed with followed tags, people, and editor choice work together.

I decided to start posting basically unabridged thoughts. Not realizing that this was actually still a young growing community. And as I learn this tool I've gotten 10 followers who are also new, did my postings really get 10 people to sign up just to follow me? 