This error came up today. If the following conditions apply to you:

* One of the first messages by git is "fatal: The remote end hung up unexpectedly"
* Using Windows
* Installed GitExtensions (are using Putty)

Then you may not have Putty setup correctly, change your setting in GetExtensions to utilize openSSH. Or fix your Putty install.