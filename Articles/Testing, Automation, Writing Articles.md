I am having a hard time creating a focused article related to testing and automation. Maybe by laying out this challenge I will have an easier time having a focus. 

When discussing automation and testing, generally, this is in reference to building out a regression suite. However you can have the computer assist by crunching numbers or gather/organizing information. 

When discussing testing you could be referring to, checking/verification, regression, or exploration/learning. But regression testing could include both the verification of existing knowledge or explanation to find new areas not previously covered. I've already written some thoughts on regression. 

{% link https://dev.to/jessekphillips/full-regression-4jm7 %}

What is so hard about writing an article is defining which combination of these items is being discussed. I could start by defining which one I am speaking to. But in that case I'm ignoring all the others and since you need to do all of the above to have good testing trying to isolate doesn't matter, except it does. 