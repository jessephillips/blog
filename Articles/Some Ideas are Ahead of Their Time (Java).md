This won't be the most insightful post, but I'll try. 

A while back in the early days of Java Sun Microsystems had an awkward idea of utilizing web browsers as a way to deliver software without the need to install it, applets.

Java needed to be installed on the client machine and the applet would run in the sanboxed VM. 

Today Web Assembly is working its way into browsers and allows most languages to run in the browser's VM. But how do these two approaches differ in the context of history. 

Back during Sun's attempt the following was true

* 56k modems (dialup) was still a large target audience. 
* Browser war of incompatibility 
* Microsoft was working on their own, ActiveX
* Java did not come with the browser or OS

Jar packages would be small but they were still large for the time and the entire program would need to be downloaded to begin using it. You would be able to open connections to the server to request additional resources, but you can't take advantage of that initial page request.

Applets just used the browser as a delivery system, they could not operate on the other pages content. This means as a content delivery system you couldn't take advantage of the work browsers already did. (html rendering inside html rendering anyone?) 

Microsoft was also in heavy competition to have the most browsers "exclusives" which meant you didn't have a clear winner on if Java or ActiveX would be available to users. Though Java's VM was making a pretty good market on the desktop. 

Security is an important consern, applets had to add additional security while Javascript had to expand capabilities.

Applet isn't restricted to a single language, but it did require the language compile to the JVM, which at time was only Java.

Back when I was taking programming classes and and coding in Java, it just seemed so natural to have Java installed. Now days I do c# and it just seems like such a pain to keep Java updated. Well in Windows anyway, Linux makes every language feel like home. 