---
title: Full Regression
published: true
description: One thing I've come to realize in software testing is to never entertain the idea of a full regression.
tags: testing
canonical_url: https://he-the-great.livejournal.com/60231.html
---

One thing I've come to realize in software testing is to never entertain the idea of a full regression. The idea behind a full regression is to collect a set of test which cover the software's functionality to be confident it can perform the defined behaviors.

The problem comes from expectations, that all test ever run will be run again, after all we said full didn't we. In an agile environment with mid-sprint, when to run this full regression also becomes a question.

Personally I'm also leaning away from this concept of defined regression testing, at least outside automated verification. Following changes and targeted exploratory testing should be preferred.