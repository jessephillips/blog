---
title: What is Important To Standardize
published: true
description: 
tags: discussion
series: standards
---

Standardization is important and I could get this commentary wrong, but I'd like to work on perfecting the thoughts. 

I think the push to standardize focuses on anything different and then the ask is to make it not different. The challenge is that you don't want to trap people.

A bad standard is the text editor everyone should use. Their are many tools that fall into this similar group. These are your toolbox tools, you've learned how to use them, they work off your environment affecting those who try to touch your keyboard. 

There is still a pull to dictate these tools. For example eclipse and NetBeans manage your project build and structure differently. These both have text insertion fields but in this case your choice affects everyone pulling down your code. Similarly if you are mocking services in an integrated environment, that service is then shared you can share mock definitions and people can understand. 

Another area of value for these tools is when training. I don't know how many times I try to help someone with Git and every graphical tool is installed because they get help from those who use different tools. So standardize on the tool used for training be it firebug or chrome debugger or postman or art.

Try to standardize communication, the how and not the what. Do you communicate through meetings, merge requests, code comments. The what isn't exactly what you think it is but first;

What you communicate across departments is important. It is like your public api where as within you department you should have fluid change to the what across departments should be more static. 

{% link https://dev.to/jessekphillips/defending-submoduals-for-git-4f5m %}

I believe my defense of git submoduals comes because it does not create a communication of what like nuget releases do. How to put this.

Remember the how, what if how you communicate was with code commits? Submodules give you the commits allowing you to see the what. With package releases you define the what to be an overview of the changes, release notes. This is rot with two problems depending on how you do this. Either release notes are manual and risk missing important changes or they are generated from your commits, which is great but now the actual changes aren't readily available.

I realize this seems like I'm slitting hairs, and I probably am. I do like the idea of packages, but in the development cycles and internal code they create a lot of administration and provide little benefit in the way of distributing change. The touted semantic versioning is the key argument for getting versions distributed so you can have updates without worrying about breaking things. Problem is you have to be diligent to follow it (unlike swagger generator "minor version with breaking changes"), and you have to actually support multiple versions, don't put all your bug fixes in the new version which has breaking changes. But with internal development supporting all these versions isn't the priority.

Wait, this post was about standardization. I don't think I did a good job of distinguishing between what I mean by 'what' and 'how'... that is unfortunate.