---
title: Using others to learn programming 
published: true
description: Learning a language in development provide important insight. 
tags: tips, dlang 
---

I have been working with and following the development of D throughout my time programming. I would get class assignments and I'd do them twice, once in Java as expected and once in D. Not every assignment, but it happened.

D was still pretty young and has had a lot of growth since. I don't think D can be used for this anymore, but learning the design choices made by the language has probably been the most beneficial to my programming knowledge. 

Jai, the language by Johnathon Blow has had it development and design choices enshrined in video. This may not be the best approach because the language isn't available to use and if it was the version shown in a video would be old and not the version to use. 

With D I've gotten to see the challenges of low level support with high level sugar. The battle of language feature or library functionality. We can create delegates 5 ways till Sunday, should we add the way C# does it? Yes, yes we should.

I think D is a great language and that is the reason I keep going back to the forums, but I can't help but appreciate the value I got out of being there for its growth. 