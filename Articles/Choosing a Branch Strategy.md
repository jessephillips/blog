In the previous article I clarified using branches for communication and Continuous Integration. Here I want to emphasize it does not matter. 

Conceptually a branch is a movable pointer to a commit. In reality that is exactly what it is. This is important because when cloning a repository you are creating a branch.

Since git maintains a list of commits, starting a new branch at any point in history is as easy as providing a name. It can point at the same location as another branch and as such is the same as the other branch.

# Define When to Branch

You will need to specify what is the appropriate branching time.

First off, your local branches are yours and you can do with them what you want.

Long standing branches are a liability. Branches should get created when you need to support/maintain codebase separate from another maintained/supported codebase.

# Marking Commits of Interest

Sometimes when you support a codebase you don't expect to need to modify it, maybe you need to go back to code which isn't supported except today it was decided to do so. 

Tags, the are pointers to commits which cannot be changed. This is a good way to mark commits which are of interest for future support if needed. I recommend having some form of tagging on any release which makes its way to production.

# Selecting a Branch Strategy Which Matches Your Support

When you read through all the approaches for Git branching, evaluate if the long standing branches are codebases your team wants to and is equipped to support, which means applying patches to without all the other work happening.


# Promotion

Concider following me if you liked the article. Check out my profile to see if you enjoy my other writings. I cover git, testing, D, and more.