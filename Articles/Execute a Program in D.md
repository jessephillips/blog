[`execute`](https://dlang.org/phobos/std_process.html#.execute) 

Another helpful tool is to be able to run another program as part of a script. This is one area I find D handles really well and allows for similar controls to shell pipes.

For this toolbox I'll stick with the basic. 

```dlang
import std.process;
import std.exception;

auto res = execute(["app name", "arg 1", "arg 2"]);
enforce(res.status == 0, "App exited with failure");

import std.stdio;
writeln(res.output);
```

This method will wait for processing to complete and provide all the std output. `spawnProcess` can be used to allow program execution while the app runs, remember to wait before your program exits. 

I like this approach because it is not subject strange escaping and quote rules from the shell. Though there is `escapeShellCommand` to help you. 

{% link https://dev.to/jessekphillips/piping-process-output-1cai %} 