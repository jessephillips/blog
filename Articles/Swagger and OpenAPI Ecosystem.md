---
title: Swagger and OpenAPI Ecosystem
published: true
description: The OpenAPI tool chain does not see the focus it deserves.
tags: rant
---

So I'm very disappointed with the swagger generation ecosystem. This goes along with a previous post. {% link https://dev.to/jessekphillips/d-does-meet-standard-stability-lja %}

When I go to grab the latest swagger generator I'm told the latest can be found on marven, nope. OK so I need to build it myself but don't have marven installed so I'll use the marven wrapper, wait where is that. OK dockers... Nope I'm in windows and still behind the times.

I'll use nswag since I'm in the dotnet world. Oh it does not understand the definition I'm giving it. OK, sure I don't understand it either.

Look I get it, and I'm not helping by just ranting instead of actually making it better. See I would like to contribute but like everyone else I don't get paid for that and in my free time I would choose D not Java. But if I made a generator in D I would be the only contributor for a number of reasons.

And yes making contributions to used open source tooling will be something I look to push once I establish some confidence in what we're building.