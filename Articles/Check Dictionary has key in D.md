I'll just take the example from adding to a Dictionary. 

```dlang
string[int][string] data;
if("hello"!in data)
    data["hello"] = [95: "value"];
```

The `in` operator is not made available to arrays. It is expected to have a time complexity of O(1) which is possible with dictionary but not a list. 