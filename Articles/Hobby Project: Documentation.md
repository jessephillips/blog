In this update several unrelated items were accomplished. Documentation was probably the most important of the changes. 

https://gitlab.com/jessephillips/devarticlator/compare/v0.0.4...v0.0.5

# iopipe

I asked on the [dlang forums](https://forum.dlang.org/post/faawejguebluwodflevh@forum.dlang.org) and the author helped me through my mistakes.

As it is a personal objective to make use of this library I made the switch.

# Misc

I have chosen to utilize the article slug for the file name instead of the article title. This is because the slug will have already worked out filesystem challenges (I think). 

I switched to an older compiler in the build system because of a bug which affects the IO library. 

# Attributes

I also went through and added attributes to denote methods an @safe, nothrow, pure, and @nogc. The use of `@` comes out of avoiding the addition of reserved words. At the time user defined attributes were under discussion with the @ syntax. Because users couldn't create these attributes we got these additions, but now they're in the same boat.

`nothrow` - methods utilizing this cause the compiler to verify that the function will not throw.

`pure` - This results in the compiler verifying the method only accesses variable passed in. This is to create a known environment so that what goes in produces the same result.

The methods I've marked are what is known as weak pure because they can modify the parameters passed in. These methods don't have multithreading or optimizations properties. What this does do is allow other pure methods which have those properties call these pure methods.

`@nogc` - The compiler verifies built in allocation calls are not made by this function. 

`@safe` and `@trusted` - This is about [memory corruption guarantees](https://dlang.org/spec/memory-safe-d.html). I won't go into details though array bounds checks remain inserted (they are removed in system code). [Safe likely to become default](https://github.com/dlang/DIPs/blob/master/DIPs/DIP1028.md).

# Documentation Comments

{% link https://dev.to/jessekphillips/comments-in-d-2636 %} 

Having achieved a direction to make improvements on, I felt it good practice to build in some documentation.

I did not write the most detailed documentation, but I provided high level purpose to the modules and elements. The module requires a document comment for the generator to pickup docs.

## Returns 

https://gitlab.com/jessephillips/devarticlator/commit/687d9ad0405976556e1419ea64352a6be69028c4#d42fd996ab88d40a393c17c6d30c03b75484e81c_58_92 

I want to note this forum post on [auto keyword](https://forum.dlang.org/post/mailman.937.1580378033.31109.digitalmars-d-learn@puremagic.com). I have returned an unnamable type so my documentation because very important as I note the interface returned is a range.

It is interesting because C# has var and Java provides a shortcut for templates. Those from dynamic languages are used to types not being specified. But types are hard to give up for those in static languages. D makes type specification prohibitive.

## Params

https://gitlab.com/jessephillips/devarticlator/commit/687d9ad0405976556e1419ea64352a6be69028c4#d42fd996ab88d40a393c17c6d30c03b75484e81c_130_182 

One key piece for documentation was having the correct cross reference. `structureArticles()` was a method created to support calling `saveArticles()`. While there was a reasonable type alias the inter-relationship provides more clarity.

## Post Call Expectations 

https://gitlab.com/jessephillips/devarticlator/commit/687d9ad0405976556e1419ea64352a6be69028c4#d42fd996ab88d40a393c17c6d30c03b75484e81c_38_62 

Some methods aren't designed to do everything. Instead we delegate the heavy lift back to the caller. In this particular case it is a file system dependency. This concept comes from a couple of thoughts, one it is easier to test the behavior when no external dependencies exist and we want least surprise. This method technically would be callable as

```dlang
auto lhs = cast(ArticleCreate) rhs;
```

To complete the conversion would mean this cast would read a file off disk which would surprise most. 

```dlang
auto lhs = cast(ArticleCreate) rhs;
lhs.body_markdown = readText(rhs.content_file);
```

It is not asking for much and an additional method can be provided to provide this complete conversion. 

Thus I need to call attention to what was not done. 