In the early days of Windows there was a very particular way to code your graphical interface. This was because all of Windows was single threaded. Your application could utilize all the processing time and thus prevent other windows to be unintractable.

The win16 api expected you to call into a special management function to release control so other window processing could take place. The win16 api had some tricks up its sleeve, as it would call this manager method for you when asking it to do something. This way there was less likely to be a rogue app.

Now days you're Windows app will need to us threading in order for your application to allow for it not to "hang". But the rest of the system carries on. 

My question then, is Web development like this old Win api? Are we using async, promises everywhere to keep our entire web page functioning rather than our component? 