When I posted about rebase I noted that understanding a commit is different from understanding how a commit addressed an issue.

I'm not saying you shouldn't seek to learn such an answer. But usually when you're in this situation you've tried 15 other things some of which may have been committed.

So here is the hard part, you need to identify the minimal modification to address your issue. This is not to say the other changes were not valuable or important, or even unnecessary to make the change which fixed the issue; what it is saying is it is important to talk to what each change did to benefit the product. 

I've done it, made so many changes, including ones I didn't know I was making and it leads me to the wrong conclusion about other changes I've made. 