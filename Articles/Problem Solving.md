I was reading this article, [Struggling With Toolchain Sprawl? You're not Alone](https://dzone.com/articles/toolchain-sprawl-youre-not-alone), and was thinking about how all the details of the poor performers make me think that behind the scenes the company's probability only fight fires on their build system. 

Which lead me to this quote. 

> And if the quick fix doesn’t solve the problem completely (it is usually unclear whether it helped or not), they leave it in place and try another solution. They don’t solve the problem because they don’t take the time to approach it systematically --https://hbr.org/2000/07/stop-fighting-fires

I believe this sums up my other article

{% link https://dev.to/jessekphillips/why-did-this-change-fix-the-issue-47h4 %}

Programming can entail a lot of trial and error, "leave it in place and try another solution" means you haven't quit identified the cause but "maybe the change is important now that it is fixed."

Basically all of those "left in" changes increase chances of breaking something else, can't be easily rolled out, and increase the complexity of review for the fix (which also may be the cause of that new bug).