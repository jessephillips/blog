There are actually a number of ways to do this, which can depend on your objectives. I want to leave you with

```dlang
import std.file;
import std.string;

auto data = readText("filename").lineSplitter;
data.popFront();
```

But this reads in the entire content into memory first. And that does not match the Python behavior I was working with. 

```dlang
import std.stdio;
import std.string;

auto data = File("filename", "r");
data = data.findAmong(["\n", "\r\n"]);
data.skipOver("\n", "\r\n");
```

There is actually a really nice io library called iopipe in the works. In my opinion it is very low level and wouldn't create a simpler example. But I have not gone beyond reading the docs.

And that concludes the series on Python. I will be starting a new series next week in the same spirit.

{% link https://dev.to/jessekphillips/multiple-series-specific-to-d-a-plan-43o6 %} 