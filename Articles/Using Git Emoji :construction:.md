In generally I have viewed the :construction: emoji as one which shouldn't make its way into the mainline, fixup commits should be applied until it is not under construction.

I have recently identify a good use for this emoji that should be preserved in the mainline history. Copy-Paste.

In this situation I have to have a duplication of another section of code and while they are similar it makes sense that they ultimately would evolve without the desire to remain consistent.

Basically it would be a commit made after pasting in some code you found on stackoverflow.com, your commit message would have the url reference etc. 

{% link https://dev.to/jessekphillips/selecting-the-right-emotion-for-your-commit-35kb %} 