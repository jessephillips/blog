# Background

I work in Quality Assurance, my heart is in programming. I didn't really view testing as a career path, I think outside of game development this is probably true for most people. 

With a little reflection, I want to make an argument for this career path if you find programming fun. 

# The Good

I find QA provides an interesting opportunity for problem solving. It is a profession where gathering information is your most valuable asset. You get to work with some smart developers and learn from them. 

How can you build out a test sequence which reproduces an issue seen only in production? How can you get closer to production?

Are you curious about performance techniques? Ask the developers about how they addressed the problem. Is UI development pretty cool, ask the dev about all the callbacks and flow of the program. Is SQL the sniz, yep there are questions to ask there too. So much to learn.

You also get to use all that programming skill to make testing easier. You can build multitheaded solutions, micro services, data transformation, OS scripts, site scraper, statistics/graphs. There are so many things software can do to help you test you won't have time to cover it all. 

# The Bad

You don't spend your entire day programming (programmers don't do that either). 

There may be some software solution to help test, but it won't be worth the effort, so manually verifying will happen frequently. 

# The Challenge

Expectations of testers can easily become script executors and designers. People think your day job is writing down what you do, reporting on it and doing it again the next deployment. 

Being able to define the best test strategy will be hard with little product experience and the experienced testers driving you down a different testing path than what interests you (this is not that their approach would be wrong, just not the one I'm advertising to you) 

You may not have an opportunity to work on issue reproduction, many times this falls on the dev. If you do take on the task one benefit to this expectation is if you are struggling to reproduce you can send it off to the devs. 