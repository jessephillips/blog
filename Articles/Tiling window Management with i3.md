# Meta 
My company has decided to embrace its employees working from home.

I usually fill my desktop with a lot of applications. Many times these are duplicates as opening something again is easier than finding the window. Working from home I have a unique opportunity to clean some of this up because my desktop is Linux instead of Windows.

I'm not not sure if you know this but Windows has had tiling capability way back in 3.1. You asked for the windows to be tiled and they would be placed one on top of the other just showing the title bar. Since this horrible idea win+tab was added. This created a mini map of all the applications.

One of the things I used a lot through college was Linux workspaces. I have since lost this ability. Something about Windows 10 work spaces isn't the same. 

# i3

Linux has had lots of different window managers, most follow a similar style but Tiling managers pushed in a different direction. 

I've never used a tiling manager before but I'm looking towards it to help me get my windows under control. Here are some of my thoughts and expectations.

## Benefits 

* It will be easier to get the most out of the screen space 
* I will prefer to close and open what I need rather than leaving it around
* it will be harder to loose track of windows 
* Task switching will be easier to identify, keep focused on on topic for longer

## Challenges 

* it will be easy to loose track of windows (which workspace, tab, stack?) 
* shortcut command heavy 
* have smaller sized windows will be harder 
* windows can't be minimized (services which do not have their own minimize to tray need their own workspace) 

I'm not new to have behaviors hidden behind commands. Vim is a preferred text editor and I've learned many command line tools. That doesn't mean productivity isn't impacted as you figure out the techniques. 

## Techniques

Just a note to self about making more efficient use of space. 

* When operating with two windows among many, then move the two to a new workspace.

# Repo

I have started placing personal notes about my customization, namely because I am a Dvorak vim user. 

https://gitlab.com/jessephillips/linuxapplicationlist/-/blob/master/i3Setup.md