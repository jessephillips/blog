This discussion was started and I think it is a good segway into a docker article. 

{% link https://dev.to/goncalodcorreia/how-would-you-describe-your-perfect-learning-strategy-383p %}

The idea for this article is to make parallels between docker and other programming terminology. 

# Images

Docker operates of the idea of having prepared operating system which can be run on any hardware. This is the same concept as virtual machines which also use the term image, but that one is too easy. 

Object Oriented Programming often provides a concept structure called a class, but a class isn't a thing until it is instantiated. And this is why class makes for a good mental picture.

# Container

Docker needs to have a running instance of its OS, that is where the container comes in. 

Containers are the objects of OOP. They derived from their class(image) 

# Layers 

The layers are the OOP worlds hierarchy where each layer builds on the previous. Each layer is its own class (image). And just like classes a layer can have multiple children. This is where space savings comes in since you only need one copy of the parent for all children. 

Now I must switch analogy to version control. A layer is a state of data for the container. It is a commit of changes on another layer (image), again emphasizing the space savings.

Like git commits these content changes are hashed and the layer or container can be referenced by the hash.

# Registry

Sharing containers is an important part of docker, registry is that repository. Why not call it a repo? I won't tell you. A registry is just there remote upstream. 

And like git the images are immutable. You can remove references to them but it will be a different image.

# Continued

Is there another concept you would like a parallel to? In docker or another subject? 