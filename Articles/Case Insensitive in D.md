Let me begin with the naive approach before I tell you to go with the naive approach.

```dlang
import std.string;

assert("I be Big".toLower == "i be big");
```

As I mentioned last time unicode isn't simple. Well it isn't easier with changing characters and this mostly isn't a unicode problem. The upper/lowercase of a character in one language changes in another. This is where localization comes into play. 

I'm actually not very well verse in this area so question my advice. If you're not working in data that crosses language boundaries then this change is mostly moot. 

I haven't seen a solution in D for handling this. I only briefly looked into it for this article. So continue using toLower as was recommended to me for Python. 