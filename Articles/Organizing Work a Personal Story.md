---
title: Organizing Work a Personal Story 
published: true 
description: Putting my desires in practice for Git management. 
tags: git
series: Git Communication
---

I've been doing a lot of code reviews, but not so much on the making changes. But my recent code endeavor has me jumping around trying to adjust the quality of the code in a number of different respects. Basically I get to practice what I preach.

I have a number of changes happening in many places not at the same time. 

* logging improvements 
* architecture changes 
* performance improvements
* bug fixes 
* attempts at bug fixes 
* refactoring 

I've been pressing for one gitmoji and a logged issue in the tracking system. All of this sucks because I'm trying to apply the techniques to the different areas of code to keep consistency, which is bringing to light the other changes needed and distracting from the work on consistency. 

I don't believe pausing to define the work for later, but I still believe it is beneficial to separate my refactoring from my fix and architecture changes.

This means I have to commit my progress often, I can take a little bit of time to group my change types and make them into individual commits. I can note issue numbers or emoji, but the main point is to get separate commits. Rebase interactive let's me break up a commit I realized later had more than one change type. I also add similar or incomplete changes together.

Some of these changes would be more important than others and so I'll can start to move those to away from the incomplete work or work that could use more intensive review.

I can finalize the commit messages and their associate issue number at the end as I prepare for a merge request. I had started with multiple branches, but due to the unfocused nature of the changes in both branches I decided to go for a single branch while I work out these final merge request details. 