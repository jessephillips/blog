---
title: Git for Data storage 
published: true
description: I've considered what git provides to use as a database for storing data. 
tags: git
---

This is by no means an original idea. I've read a few things such as [Git as NoSQL Database](https://www.google.com/amp/s/www.kenneth-truyers.net/2016/10/13/git-nosql-database/amp/).

Why even consider git? History of data can be very valuable and git facilities tracking changes with many tools to review such changes. I thought if I made use of the database directly rather than through the filesystem I could get some nice advantages. 

You don't want to consider git with highly cross-referenced data that would require complex joins. Git is designed te efficiently store data anh not duplicate, so it can query history and the meta for that history.

I'm still pondering the use case for this, it requires history to be most valuable. But one thing is certain the data must work off a filesystem as the git primitive storage, blobs and trees, map directly to it.

My original thought of application was test results and artifacts. Branches could be useful for environments, but managing association with other environments could be useful but hard. We'll see if I ever try to go further. 