Automated testing is a liability. Manual checking functionality is a huge liability. I want to emphasize some concerns I have with the machine learning testing solutions.

If you are not familiar with testing vs checking here is my quick explanation. Computers perform checking, humans when they are not following a script perform ~~thinking~~ testing.

The primary issue I take with the machine learning automation is it tends to focus solely on web frontend verification. For certain websites this my be sufficient, however I think that is likely very few. In an earlier article I described a web UI testing setup I'm using. 

{% link https://dev.to/jessekphillips/wiremock-as-a-web-server-for-testing-3lkl %}

Machine learning would not allow me to use this method of control. It also does not focus on other validation like database reporting.

There are a lot of moving parts when it comes to a "working" application. If your test strategy could be replaced by the machine learning tools of today, then it I claim that wasn't the right strategy to begin with.

However machine learning is a valuable endeavor and could use some funding. Even though I don't view it as the right strategy, it is better than nothing and perpetuates the most common test strategy. 