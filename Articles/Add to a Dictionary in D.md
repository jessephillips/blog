Dictionary, hash table, associative array, map it has many names. The main challenge with dictionary adds is more around nested associative arrays, are you going to be annoyed if I select a different name every time?

```dlang
int[string] dict;
dict["Key"] = 55;
```

There is a Dictionary literal in D. 

```dlang
auto data = ["key" : 55]
```

but this is not good for adding data as it will replace the variable with a new object. However if you're nesting and the inner dictionary might be null it can be good to use for starting a dictionary.

```dlang
string[int][string] data;
data["hello"] = [95: "value"];
```