---
title: Do we really need POJO? 
published: true
description: I just want to point out the heritage of POJO. 
tags: pojo, pod, terminology 
---

Plain Old Data, what is it and why do I care. Well I'm a little annoyed because Java has decided that isn't good enough for them.

See POD aren't objects they don't have inheritance, they aren't references. Java can't do PODs. So why the annoyance with POJO. I think it is because spirituality they are the same and a language like D which can do both would have little reason to make that distinction because you would not reach for POJO. Even if a need existed it would probably be PODO (plain old D object). 