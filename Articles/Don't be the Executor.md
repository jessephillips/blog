---
title: Don't be the Executor
published: true 
description: Off load execution to the machines, it will create visibility and documentation on how. 
tags: testing, regression
---

This is a post for testers, equally applies to devs but they already do this. 

Don't be the guy who opens up the test tool, loads in the test data and clicks the run button on your tests. (feel free to mentally replace test with check) 

It is pretty standard for developers to stand up a build box and have their code compiled and unit tested on a remote server. It is also common to only accept builds from this server, never sending a package off the developer computer to production. 

But in testing this is the norm. Results are produced from a tester machine, their settings and their test data, many times the test results are fixed up after manual testing before anyone else sees it.

Instead look at having tests executed by a different system. If that means setting up Jenkins separate from the devs do it.

I decided to write this after seeing 

{% link https://dev.to/dowenb/two-simple-tests-you-should-be-using-with-postman-53hj %}

And if you're going to us postman you will need [Newman] (https://github.com/postmanlabs/Newman).

# Promotion

Concider following me if you liked the article. Check out my profile to see if you enjoy my other writings. I cover git, testing, D, and more.