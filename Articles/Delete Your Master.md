---
title: Delete Your Master 
published: true
description: You don't need your master anymore. 
tags: git
---

Distributed version control comes with many ways to organize your code. The master branch is by convention the primary focal point for development. This could be a stable or shared hod podge of integration. 

In either case I recommend that you delete your local copy of master, leave the remote branch. When you you are wanting to start work fetch remote and checkout origin/master (or whatever your working branch is).

Yes this puts you into a detached head. This isn't an issue, git will let you build your branch any time. And it is much easier to name a branch after you have some work behind it to name it.

# How is Local Master Used

Pull requests killed the local master branch. The purpose for this branch is to work out a merge of branches for other work happening. And it is still a valid option picked up by Github. 

Pull/Merge requests provide a web interface and will perform clean merges. Merge conflicts are expected to be addressed in the work branch, and one reason rebase is preferred so many places.

I recommend deleting your local master, but if you have a need to control master locally, by all means bring it back. Just remember git will manage origin/master so you don't have to. 