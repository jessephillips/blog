Producing content whether it is source code or articles, obtaining consumers requires some advertising to reach an interested audience.

I've created content on dev.to for the last year. It provides a small amount of advertising with a recent article feed and tags. New signups also get a suggestion to follow recent contributors, this is where the majority of the followers I get.

# Reading Articles 

As a consumer of content I find it hard to determine good authors to follow. Reading one of their good articles does not mean they generally write in topics of my interest. If I don't look at the author I won't notice pattern of reading articles from the same author. 

For this reason I try to look at the profile of authors with good articles. This way I can identify if the pattern of articles is relatively capturing my interest. 

# Producing Content

So now I have some followers, many of which may have little interest in my content.

I have about 100 articles. New followers are unlikely to find articles in the group they are interested in. Searching by tag and author does not appear possible.

Dev.to provides article pining on the profile. This can help, but is not the quite the same as it highlights my interests.

# Historical Content

My previous articles become a place of reference. Both to use in response and recollection of commands.

 https://year-in-dev.cephhi.com/stats/jessekphillips