I started a series based on the searches I performed to work in Python. I liked the results, but since I'm not asking questions of Python it misses the point of that series.

{% link https://dev.to/jessekphillips/boolean-in-d-943 %}

Thus I've decided on introducing three different series to continue in its spirit. 

* Toolbox
  * I will use this will focus on small tasks, simple solutions. 
  * {% link https://dev.to/jessekphillips/parsing-a-string-in-d-15pk %} 
* Workshop 
  * This will focus on items which are more complex. 
  * {% link https://dev.to/jessekphillips/argument-parsing-into-structure-4p4n %} 
* D Features 
  * These cover features provided by the language which support solutions but does not directly applying the feature to solve a problem. 
  * {% link https://dev.to/jessekphillips/templates-3dnf %} 

{% link https://dev.to/jessekphillips/my-case-for-d-444p %}

I also signed up for [ko-fi](https://ko-fi.com/jessekphillips) but I'm not sure if they are more than just a glorified link to PayPal. Maybe if I placed my content there... 