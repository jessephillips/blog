---
title: So you Programmed in a Regression
published: true
description: 
tags: git, regression
series: Git Communication
---

Did you have a feature which worked perfectly and now it is a piece of garbage, riddle in a hidden bug? OK that was harsh, the point is it would be nice to know what changes introduced the behavior difference.

Git bisect performs a binary search of your commit history. Start by finding a known working commit then use bisect to manage the code checkout to find the affending commit. 

By following a communicative commit history, each commit should compile, and you'll be told what was requested at the time.

See these tools aren't the first thing to reach for, but they can be an effective way to track things down when it the issue isn't directly pointed to. The D community put together an additional tool to track down compile regression by supplying a command which identified the failure. Git bisect was executed to automatically go through compiler builds (from Commits) to get to the introducing commit. 

{% link https://dev.to/easyaspython/hunt-bugs-with-git-bisect-l44 %} 