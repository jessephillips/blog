If you read my profile, I say that I'm trying to establish more involvement in communities. I wrote that with my professional title of quality assurance in mind. Well outside of work I all around prefer to code in D. One of the particular criticism is that not many projects show up compared to Go or Rust.

I made a post about command lines argument parsing, it didn't, initially, end up in github thus contributing to this perception. 

# Personal 

Well [struct opt](https://code.dlang.org/packages/structopt) is now a project. But not just to promote its awareness I want to speak of the difference in effort.

## Solving a Problem 

This was the biggest effort. I've been using D for most of my days programming, I've grown in my skills with the language itself. (I read someone mention this is why Go is nice it is a small language, so initially it is easy to learn and as the language gets bigger the bites are smaller to chew) 

Putting together the code to help for where I wanted to use it didn't take much, but I did need the time I already put into the language to know what and how it was possible. 

I invested in language knowledge, but many invest in library knowledge, arsenal of solutions. I think D is very well suited to writing libraries because it makes writing generic code readable (with copy/paste or layers of indirection with loss of typing). 

## Article 

The article took a little bit of effort. I wanted it more general and I had to describe what are the moving parts of the language. But it was still isolated to what was done. 


## Project

Shifting to a project is another matter. I needed to put it into the building system, what name is this project? I should have a little bit of organizing the code. I'll want to document the public interface, and the private ones for hope of contributor sanity.

It also actually needs to be generic and tested for that generic capability. This also wasn't too challenging. 

Then it needs to be robust. In this particular scenario of generating code into another api, I would like my library to be able to express the same capabilities. I believe this is where I am left with today, with need to improve documentation more.

# Existing

Contributing to D isn't foreign to me, std.csv has my name on it. I also managed a couple library reviews.

## Libraries 

I wanted to try some dev.to integration which has an OpenAPI spec. D has a library but the documentation is lacking and it looked more like a way for generating api handlers rather than requests. 

I was planning on contributing, but as I had more time to think on it, this wasn't the way I wanted to provide an api for OpenApi. My thought is related to generating D classes with attributes (but really this is an experimental thought), but then I need an OpenAPI spec parser... 

See outside of work I'm wanting to play with ideas, contributing to something which doesn't play well with my thoughts so I can play with something else is not motivating. 

I have had fun with other contributions like LuaD and protobuf. 

## Language 

Contributing to the language and standard library is getting more and more difficult. Aside from meeting quality and completness requirements it needs to adhere @safe and @nogc with a preference to be nothrow and pure.

This isn't dig on the direction or requirements. But it does make it hard to add to it. That is were contributing to a third party repo can allow for access to libraries which can grow and provide vetting for what the community wants.

But there are side effects, which argument parsing do you choose, getopt, structopt, or dargs. If each has drawbacks or incomplete in some way what would you choose to contribute to?

I believe other communities have avoided this problem in 3 ways.

* They really haven't
* The infux of new users creates a bed of work new users continue to build on. 
* The audience is smaller in scope. 

D has made an appeal to many, it has low levels and high levels. It would work well in embedded but also for web services and mobile. It could tackle gaming and data science. This makes for a very diverse target audience.

## Documentation 

Coding isn't the only place contributing can occur. Improvements to documentation is also of value. Articles are an easy way to get information out there which is helpful, but contributing official documents is a larger effort. Even with politically neutral changes like spelling corrections, contributing still requires pulling down the repository finding the file needing modified. If there is an issue tracker then you may need to find and associate the commit. And if the community uses gitmojis, you may need to find the right icon for your changes. 

# Conclusion 

Being a contributor is a scale. It depends on what and how you contribute to determine the time and effort to make a good contribution. Your skill level does not mean your contribution will be good or bad.

I'm currently looking at proposal of a standard attribute set.

https://github.com/JesseKPhillips/DIPs/blob/serialize/attribute/DIPs/1NNN-jkp.md