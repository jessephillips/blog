This post is a language comparison coming out of this great article on list comprehension. D does not have List comprehension.

{% link https://dev.to/dvirtual/list-comprehensions-om4 %}

Since D can generally operate on ranges rather than allocated arrays, if you need an array just add `.array` for more on arrays review 

{% link https://dev.to/jessekphillips/slicing-and-dicing-arrays-5akg %} 

My explanation will be limited to D specific differences, but please ask for further details if something is not clear. 

# List

```dlang 
import std;
void main()
{
    // arr = [i for i in range(10)]
    writeln(iota(10));
    // [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
} 
```

Iota is uncommon, but is the equivalent of range in Python. 


# Dictionary 

```dlang
// y = {i:v for i,v in enumerate(x)}
auto x = [2,45,21,45];
auto y = enumerate(x).assocArray;
writeln(y);
// [0:2, 3:45, 2:21, 1:45]
```

As mentioned D prefers unallocated range manipulation, this tends to mean no index, enumerate creates a tuple with a count and value, and `assocArray` takes a range of tuple to build an associative array (dictionary)

# Conditionals

```dlang
// arr = [i for i in range(10) if i % 2 == 0]
auto arr = iota(10)
    .filter!(i => i % 2 == 0);
writeln(arr);
// [0, 2, 4, 6, 8]
```

```dlang
// arr = ["Even" if i % 2 == 0 else "Odd" for i in range(10)]
auto arr2 = iota(10) 
    .map!(x => x % 2 ? "Odd" : "Even");
writeln(arr2);
//["Even", "Odd", "Even", "Odd", "Even", "Odd", "Even", "Odd", "Even", "Odd"]
```

# Nested for loop

```dlang
// arr = [[i for i in range(5)] for j in range(5)]
auto arr3 = iota(5)
    .map!(j => iota(5));
writeln(arr3);
// [[0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4], [0, 1, 2, 3, 4]]
```

```dlang
// arr = [(i,j) for j in range(2) for i in range(2)]
auto arr4 = iota(2).cartesianProduct(iota(2));
writeln(arr4);
//[Tuple!(int, int)(0, 0), Tuple!(int, int)(0, 1), Tuple!(int, int)(1, 0), Tuple!(int, int)(1, 1)]
```

I find that D makes this behavior very clear. 

Tuples being a library provided type, their string representation is a little more verbose. 

# Flatten 2D Array

```dlang
// arr = [i for j in x for i in j]
auto x2 = [[0, 1, 2, 3, 4],
 [5, 6, 7, 8, 9]];

auto arr5 = x2.joiner;
writeln(arr5);
// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

# Conclusion 

D is a typed language, since I did not convert everything back into an array a unique arr variable was needed for each. 

Personally I think D represents the behavior more clearly than Python's list comprehension. Even the conditional output selection, which was more consice in D was represented reasonably. 

This does not touch on the other algorithms D provides and can be applied to the ranges.

Python is praised on its clear syntax and readability, well I must be spoiled because it makes me cringe on most everything I see. 