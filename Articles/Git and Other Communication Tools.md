---
title: Git and Other Communication Tools
published: true
description: 
tags: git
canonical_url: https://he-the-great.livejournal.com/60925.html
series: Git Communication
---

My last post on Git tried to focus on an end goal for your history. But I can already hear the nagging reply's "We have other tools which we use to communicate these things."

Most likely you're utilizing some form of bug tracking system, these systems help you to organize what is changing and communicate back what has been completed. These tools are very important as they provide external input, records of future work needed, and facilitate planning efforts. They can also facilitate pretty graphs and development cycles.

In my view, these tools make the reality of clearly communicating changes in your commit history even more important. In all likelihood your git commits can be integrated with your bug tracking system, not only to leave comments on the issue/bug but to cause resolution/closure of them.

If your read my last post, I referenced that during work on one feature some additional bug may be identified and fixed. When you break this fix out into its own commit, you'll start to wonder, "What issue number do I reference in this commit?" The answer to this question will likely result in, "I need a new issue number to assign this commit to." Otherwise if you're doing code reviews, the reviewer will look at your commits and ask, "Where is the record this work was done, I don't see an issue number assigned?"

By communicating we have better checks and balances and expectations on what and where we communicate.