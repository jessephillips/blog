In my continued attempts to write in Python, I desired to comment out some code. I tried an assortment of styles including ; but I still had to search because I had forgotten #

D follows the C++ comments.

```dlang
// single line
/*
  Multi-line
*/
```

It goes a step further with nested comments. 


```dlang
/*
/*
  Multi-line
*/
This is not a comment 
*/

/+
/+
  Multi-line
+/
We are still a comment
+/
```

If you place an additional character in the comment start, then these comments are eligible for documentation generation.

```dlang
/// Document comment
/** doc comment */
/++ comment for docs +/
```

Well D does not stop there and provides a different way to prevent code from compiling. 

```dlang
version (none) {
    auto var = 85;
}
```

It is still run through the parser, but otherwise is not required to compile successfully.