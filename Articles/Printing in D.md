Another excellent article on Python has me questioning why.

{% link https://dev.to/alamarw/python-s-print-does-what-54if %} 

`print("Hello Word”)`

```dlang
writeln("Hello Word");
```

This is where the similarities end. `writeln` is not a substitute for `print` as we see next.

`print("my", "name", "is", "Austin")`
`# my name is Austin`

```dlang 
writeln("my", "name", "is", "Jesse");
// mynameisJesse
```
While multiple arguments can be passed in, no default or explicit separator exists. Instead we can wrap our call in an array to get access to some array ops. 

```dlang 
writeln(["my" , "name" , "is" , "Jesse"].join(" "));
// my name is Jesse
```

`print(“my”, “name”, “is”, “Austin”, sep=“,”, end=“!”)`
`my,name,is,Austin!`

```dlang 
write(["my" , "name" , "is" , "Jesse"].join(","), "!");
// my,name,is,Jesse!
```

In order to replicate the Python behavior, `write` was chosen because `ln` stands for line which is placed at the end. The exclamation mark is then included as the last argument. 

`with open(“text.txt”, “w”) as text:`
`    print(“my”, “name”, “is”, “Austin”, sep=“,”, end=“!”, file=text)`

```dlang 
with(File("text.txt", "w"))
    write(["my" , "name" , "is" , "Jesse"].join(","), "!");
writeln(readText("text.txt"));
```
Or

```dlang 
append("text.txt", ["my" , "name" , "is" , "Jesse"].join(",") ~ "!");
```

This uses a tilde (concatenation operator) because append does not take additional parameters like write. 

https://run.dlang.io/

# Conclusion 

Python's choice to place a space between prints arguments likely lead to the choice of giving control of that separator. While the code to replicate Python's behavior is only minor, by making the default behavior meaningful to those wanting something readable printed to the screen Python required less familiarity of other language constructs.

If you instructed a new programmer "just wrap all your arguments in these brackets and call this join like so" as done in D, that new programmer would look at you and say "it's a computer why doesn't it do what I want instead of jumping through hoops. 

Where I look at Python and see learning isolated tools. D's approach (also available in Python) is universally applicable. It can do a console and file, and other strings, networking, in some cases datatype. 

Python tries to do the right thing but I think that opens the door for confusion because being explicit is not the norm. 