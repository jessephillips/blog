---
title: Utilizing Webassembly from D
published: true
description: 
tags: webassembly, dlang 
---

So I really haven't gone down this path, but it corresponds to my other post for D, I don't really see too many accessible libraries for web assembly; yet here D is with something making good headway. 

Spasm looks like it has implemented the translation for basic types utilizing a GC restricted subset of the language.


https://forum.dlang.org/thread/fgzmgdhazqungwqjqves@forum.dlang.org

On another note anyone know what Go is doing to avoid its GC? 