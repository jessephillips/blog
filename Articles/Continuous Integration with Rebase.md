In the first post I detailed out what it takes to achieve continuous integration and build servers are not required. 

“continuous integration (CI) is the practice of merging all developers' working copies to a shared mainline several times a day." -- [Wikipedia](https://en.m.wikipedia.org/wiki/Continuous_integration)

So what is the problem this is solving? Wikipedia continues to provide a little insight here.

"The longer development continues on a branch without merging back to the mainline, the greater the risk of multiple integration conflicts"

Note that this conflict goes beyond merge conflict but also feature conflicts where the functional you developed and tested do not work when this other feature steps on your expectations.

{% link https://dev.to/jessekphillips/handling-merges-464d %}

Rebase is a means to pull in changes from mainline. I say 'pull' but I do want to note this is not an accurate model for thinking of rebase. By rebaseing your work onto mainline you will be able to continuously integrate the work of others.

Of course others won't be integrating with your work, thus short lived is still important.

# Interactive Rebase

Merging in mainline would have similar integration benefits. So why rebase?

Rebaseing will encourage an understanding of your changes, encourage simplifying history so that reviews can focus on how your changes integrate into mainline. If you changes need to apply to another supported branch they are already simplified and grouped together for applying elsewhere.

{% link https://dev.to/jessekphillips/interactively-rebase-with-git-4o7n %} 