This requires a little bit of background. Our product is sold highly customized so we utilize a forking model for development. The UI has a clear interface to the backend (no backend rendering), there are only a few files (not a sprawling website of content).

---

Wiremock provides a http(s) server which will respond to a request based on matching criteria (this can be the url, header, or body content). 

Wiremock exposes a rest api allowing for the mocking to be defined with the tests and posted to the server before test execution (or as part of). This keeps the test self contained, preventing the need to maintain a "test" environment. 

Multithreaded testing can be challenging in this situation because the mappings can conflict with each other if the match criteria is not sufficiently unique to the test session. 

---

Wiremock is our chosen mocking solution. We have hosted our product in a docker container with Wiremock serving the UI and the UI configured to communicate back to wiremock for all requests. This provides a self contained UI build where our selenium tests can handle and mock all UI interactions.

To add this testing into a pipeline, we needed to stand up the container and have a predictable url. This meant we have Nginx provide redirection based on project and branch. It also provide an environmental config our test solution utilizes. This is needed because we host for many projects and branches, so the container selects a random port. 

---

To provide pre-defined mappings. Start from the docker image. You then need to copy over the [mappings](http://wiremock.org/docs/stubbing/), and duplicate the [entry point and cmd](https://github.com/rodolpheche/wiremock-docker/blob/master/Dockerfile#L37) 

{% github https://github.com/rodolpheche/wiremock-docker %} 