---
title: Leadership with limited perspective
published: true
description: Being looked to lead QA when development increases the complexity and scalability of testing and development. 
tags: leadership 
---

Transitioning into a leadership role. This is quite an interesting challenge. I have to not only learn a new set of skills, but leave behind doing most of the work, guiding others with no authority of my own.

I am a professional software tester, not developer. And even then my test experience comes from one company. I am in the best position to take lead but lack the holistic experience to be viewed as knowing how to effectively manage code.

I'm interested in being challenged and learning.

I have to take a highly customized platform and test each customized implementation at scale. I can't expect that an implementation will stay up-to-date with development of the components which build the custom implementation. 

We utilize npm/nuget packaging to manage injecting customization. This requires there be a solution that brings these packages together.

I have brought up the use of forking and maintaining updates through this old concept of version control. I've also mentioned that submoduals facilitates much of the challenges we address with package managers. This leads to accusation of archaic coding practices and there are reasons everyone is moving to package management.

Now I totally think nuget and npm adds complication for what we deliver, so let's ignore that I'm only talking about what to do with the implementation repos.

See architecture is not tied to how the code is built and packaged. That means you can have a highly decoupled code even if it is in the same repo, I'd even argue that can still be achieved in the same file with the right language. The challenge we face which we are looking to solve with packages is managing changes and delivering changes. Sem versions are of course the solution to making changes that don't break anything (remember I'm QA so that means I get to test for that feature).

For me git was made to manage changes, communicating changes, and I think it helps reducing differences when used appropriately. See I posted about the first two in a previous post. By having the full source code in one location allows for deep coupling and uncontrolled change conflicts.

This is where my experience falls short, how do I create a system or process that facilitates good architecture which make managing changes easier. Yes it can be distilled down to old, tried before version control. But my research suggests this is just as new as cpan is to Javascript. See I do know my history too.

These ideas are not me against the devs, I do have support from some within development and progress is being made in this direction. 