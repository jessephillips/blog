If you read my previous post which focused on test data for this functionality, you may find the effort disproportionate. 

https://gitlab.com/jessephillips/devarticlator/-/compare/v0.0.6...v0.0.7

```diff
auto isNewerArticle(ArticleMe am, PullState ps) {
-    return null;
+    return am.id > ps.lastArticle;
```

That is the implementation. Given an article and the previous pull data, the newer articles are the ones with the greater ID number. 

This assumes articles are given newest first and that article IDs are given increasingly. However if the first isn't true I won't have reason to attempt short circuit.

------

```dlang
alias inPreviousPull = (x) => !x.isNewerArticle(lastPullState);

devreqArticleRange.until!inPreviousPull
  .structureArticles
  .saveArticles(savePath);
```

You won't find the above code in the changes, I've taken the unittest and pulled out the complete concept to save new articles.

If I abstract the dev api into a range, I'll be using a normal range sequence and combined with the components I've already tested.

What is miss here is storing and retrieval of this lastPullState.

------

I wrote the test first. I almost didn't because the implications was so simple, but it is good to see the test fail.