After reading this article on "[Why CLIs suck, GUIs are better](
https://developer.okta.com/blog/2020/02/19/why-clis-suck-guis-are-better)" I wanted to write up some of my thoughts on the statements and specify my own position.

# The Accurate

The benefits for CLI tends to be mentioned as good for automation. Nufsaid. 

GUI makes it easy to learn through exploring the menus. I have a story to emphasize how valid this statement is. 

My college required passing a computer literacy test. It was possible to test out of the class and that is what I chose to do. 

This test utilized a replication of MS Office applications, asking you to perform a task and giving you a limited number of attempts. Access was one of these programs. While I didn't do every task requested in Word or Excel I had never touched Access before (freshman in college). Due to the graphical interface I was able to work my way through to passing even this portion of the exam.

Then MS released the ribbon and that ability went out the window, and maybe the college learned that was a bad way to test people. Basically GUIs can make self learning a thing but then you have [Git extensions](https://dev.to/jessekphillips/i-find-git-extensions-confusing-312n). 

I have never had to take a similar test using a CLI, but I can imagine reading through man page or help could be annoying. But I generally use Google, which sometimes does end up in a man page. 

# Wait What

When I started reading the benefit to the GUI I started to question if the article was taking a serious position. I'm interested in trying to write this without directly speaking to glaring problems as I wish to try and educate in a different way.

# Helping Others

When attempting to help solve someone's issues I claim the CLIs are easier. Taking screen shots, giving step by step clicks and key strokes that is hard.

Then you have "which GUI are you using?" because there tend to be so many. 

In general, you can rely on a user being able to run the terminal command you provided. Sure you could write up a fish script or GUM command, but there is a reason those don't get much appearance in places like stackoverflow. 

# Remote Access

CLI was designed for remote access, well the terminal more specifically. Actually X11 was also designed for remote viewing, but it required a lot more data transfer and probably had some flaws in implementation.

Seriously if you're used to terminal, browsing the web, editing files, writing emails, they don't become harder remotely.

# Screen Readers Read Text

CLIs tend toward a textual representation of data. It requires the author to design their output in a friendly format for reading. 

I'm not familiar with screen readers in the CLI, but I can guarantee they would have a much simpler time if every website user was navigating websites with a keyboard and a terminal.

# You Should Use a CLI

CLI is good for automation, if you're reading this it is probably accurate to claim your a programmer and telling the computer what you want it to do is like in your job description. Making those steps repeatable is just the day job.

I've learned some bash scripting, I know many Unix tool commands, I'm a vim user. But I never really learned sed or awk, grep I know a little bit. I utilize ffmpeg when I get value out of it, I search for tools that help when I need help. Long gone are the days I remember the specifics of commands so I can invent solutions to solved problems. 

# You Should Use a GUI

Graphical interfaces should not be avoided. Using a GUI does not make you bad at anything.

GUI excel at graphical representations. They can better present information in an asthetically pleasing way. With the right design they can facilitate learning how to use them, or present familiarity from real world objects.

I use graghical interfaces a lot, I also use the CLI a lot. I'm greatful for having easy copy and paste instructions of the terminal. I love having the simplistic navigation provided by a graphical interface. 

CLI & GUI affords you a lot of power, stop trying to stick with only one. 