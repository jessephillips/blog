---
title: Beyond Static Typing
published: true 
description: 
tags: dlang 
---

I've made a couple of posts about D and there is actually a common theme. My case for D goes beyond a static scripting language. If you read my covariance article what you see is a technique to getting more out of static typing. 

{% link https://dev.to/jessekphillips/covariance-and-multiple-interface-inheritance-4n3f %}

But D does not stop there, it provide arbitrary compile time execution of code. It has expanded in how much of the language is accessible at compile time with specific limitations on system calls (disk access).

There has been some very nice utility to compile time libraries like pegged and regular expressions. But this facility can also be used to write static unit tests. Now as cool as that sounds how do I explain how different this is from a unit test library that validates after compiling.

This is tricky because D facilitates verification in a number of different ways. Unit test blocks are built to the language, in and out contracts, class inveriants. When you consider all the places and features covered in the language have additional times to execute and stop a build the better.

I don't believe TDD produces good design, I practice DRCT (Design by Runs at Compile Time). 