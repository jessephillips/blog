If you take a read through these 
 [Automation Mistakes](https://techbeacon.com/app-dev-testing/top-7-test-automation-mistakes-how-avoid-your-next-fail) you will find it is mainly targeting management. I was hoping for something a little different, but since we're on the subject.

The article mentions the mistake of automating the same test script steps currently done by the team. And it hints at the use of passing everything through QA. I want to elaborate here on what I think is important to keep in mind. 

First off, if you are needing to have your humans run test scripts you're using your human wrong. Minimum wage laws are preventing you from hiring people who don't think so give them a challenge they need to think on. 

This first step however can't be done in isolation as the test script execution approach expects a specific flow to the development release. To turn your employees into thinker, ones that use automation or not, they can no longer sit at the end, nor can they be expected to verify your application has no regressions and is working as defined.

That sounds pretty bad, didn't you hire these people specifically to identify regressions and verify functionality because you can't trust a developer to test their own stuff. And this is why agile calls everyone a developer, you want a team responsible for the development and quality of what is released. In all likelihood if you are investing the use of automation then your expectations on what these people were hired for is showing to be wrong.

Let's pretend for a moment that you have a team handling all regression and new functionality validation, keeping defect escape and delivering time low. You see automation as purely a way to prepare for growth. My advice, leave that team alone and hire a new team for automating the growth. 

Back in the land of reality you likely have a diverse team of different testing skills. Their success or failure probability changes per release, their may may be additional hands looking at the release before and after QA. The QA team is pressed for time and never gets through everything.

In such a world is an expectation of learning good software development or test automation technology while releasing new product functionality, all while debugging the test automation still in development or has inconsistent results, is that going to be realistic? 

Automation is a big expense, you don't want the cost going only into QA development, put it into the release pipeline and infrastructure. Doing so provides a good place to discuss specific improvements. 