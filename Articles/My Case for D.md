---
title: My Case for D
published: true
description: I ask my self, is there a reason I shouldn't use D.
tags: dlang
---

D has been a part of my programming life almost from the beginning. I started with ZZT an ascii computer game, HTML played a role in my development but ultimately chose perl as my first programming language while a friend picked up BASIC. I read through most of "Teach Yourself C++ in 21 Days." All of this was in High School. I finally took a course in Java and on of the things I did was look for a better language than C++, I came across D and I'm talking [old docs](https://web.archive.org/web/20021205114505fw_/http://digitalmars.com:80/d/index.html). But I wasn't ready for a language this young. Finally a few years later I went to look for that better language again (couldn't remember the name for the life of me) and it has been with me ever since.

[D](https://dlang.org/) has become my scripting language. When others would reach for a dynamic language, I grab D. It is also a language I get to use to explore other areas of programming: different programming techniques, different programming layers (compile time vs runtime), manual memory management, C, DLLs, COM, Webservers, GUI.

See, the graphical side of D still hasn't reached a reasonable state even today. But there are bindings and it can be done. Outside of that though, D has created a rich capability as a Swiss army knife. I have to test our software, which means doing some very innovative stuff like writing a [MSI Custom Action DLL](https://he-the-great.livejournal.com/58420.html). On top of that I need to inject it into the MSI and Orca wouldn't do when the setup would be automated and that meant oven more Win32 API calls for MSI manipulation. While I know I could write the DLL in C# I'm not sure if manipulating the MSI could have been done outside C, C++, D.

[Programming in D](http://ddili.org/ders/d.en/index.html)