I didn't search this one for Python, after my last post I thought it important to call out an unexpected behavior when using std.algorithms with strings.

Remember the grapheme note, well in the original range design for string, it was thought that a 32bit UTF character was a complete visual character. Grapheme this was not the case. 

This results in going from `char[]` to a range of dchar. While this works for continuing with the algorithm, generally by the end you want a string again.

```dlang
import std.range;
import std.conv;

string s = "foo".take(2).to!string;
```

As an aside, the examples I've given throughout this tutorial have all been written on my phone without a compiler. In general it should be possible to just copy them into main and it will work, except when calling unimplemented function. Apologies if they don't compile. 

```dlang
void main () {
} 
```

https://run.dlang.io/