---
title: D does meet standard stability
published: true
description: The D programming language has generally been criticized for its unpolished ecosystem, I think they're wrong. 
tags: dlang
---

I've been coding in D since 2006. I've seen a lot of its growth and pains. One of the challenges has been been the out of the box experience.

Back in the old days you'd get a zip file and extract it. That was just too simple. The compiler would compile files you give it, that is confusing, why do I need to spell out what the compiler compiles.

Generally Visual Studio is the example of out of box stability expected by Windows developers. Normally I've agreed with this, things generally work.

But recently I've been put into more collaborative expectations. And I've found people can't deal with update. Any issue is the fault of the update, when there are update issues it's usually because only one of many needed parts are updated. Library dependencies stagnant.

When looking at the web development front. Builds are a collection of languages, tools, conflicting frameworks, and compile times longer then their compiled language counterparts. 

I would not suggest D avoids this if used, but I can say with confidence that the rest of the development world didn't solve the problems either. 