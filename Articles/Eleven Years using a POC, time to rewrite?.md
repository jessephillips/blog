I read a lot of code. I also have done most of my programming solo. I don't think it was clear to me how much code of others I would read in comparison to others.

I would see examples for solutions in one language and try replicating them in D. I would utilize projects and scripts written by others but not maintained and would debug and fix issues. 

I built a test script POC back in 2009 (11 years ago) and that POC, with years of maintenance and growth, is still providing me confidence that the project will function in critical areas as expected. 

Well the POC is up for a rewrite because it is written in D. Setting aside bias in this is hard, but I did not rewrite the excel sheet with VBA scripts, someone else did. I didn't rewrite the VB gui that replaced it, though I did start explaining logic by putting it in C#. I did rewrite some D scripts to msbuild, then went back when no one helped. I have rewritten some scripts from powershell and Python because they weren't setup to work with the deployment sequence they would be a part of.

-------

OK, so bias aside... 

The code base is no longer a POC, like most POCs it has made itself into a production like solution. It has documentation, unittests, build system, and has been through cycles of bug fixes.

If we look at any particular validation it would seem easy to get started, but to build out the library system to have these validations work together and share effort, that requires a plan and architecture and vetting. And of course I'm asked to ignore functionality used by the project which utilizes the most functionality. 

Then you add in the testing effort. By ignoring functionality, the costs can be reduced in the short term but harder to track as at that point you're not looking at the prior implementation.

-------

On the other side. No other developer or tester has used D. To support the tool, some new knowledge specifically around D would be needed, probably even some investment on personal time.

There would be a need to learn not only where code lives which performs certain actions, but what it means for code to execute at Compile time. Compiler errors will be different then they are use to.

-------

People aren't like me. Every time I look back at the code to concider rewrite, I see all those hours to build, simplify, and debug, and I just don't want to do it again. Am I wrong? I think someone else should help out maintaining and at that point (with the knowledge of what is in the system) they decide. 