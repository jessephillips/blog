[`mixin`](https://dlang.org/articles/mixin.html) 

D provides two features under the name mixin template mixin and string mixin. I will only be covering string mixin at this time.

The idea here is to take a string which defines valid D source code and inject it for compilation. Let's start with something simple.

```dlang
mixin("int a;");
mixin("a = 6;");
assert(a == 6);
```

This is not very effective practice for standard coding. This is not the same as macros like those in C, D requires the string to contain a complete statement(s).

```dlang
if(mixin("a == ") 6) // failure 
```

D won't just inject the string into the code. It requires a complete declarations, statements, or expressions. 

It is possible to use these generated code blocks with CTFE.

```dlang 
int addTwo(int a) {
    mixin("a += 2;");
    return a;
} 

static assert(addTwo(4) == 6);
```

# Operator Overloading

[Docs](https://dlang.org/spec/operatoroverloading.html#binary)

```dlang
void main()
{
    auto a = new T;
    auto b = new T;
    a.data = 1;
    b.data = 2;
    assert((a + b).data == 3);
    assert((b - a).data == 1);
}

class T { 
    int data;
    T opBinary(string op)(T rhs)
    {
        T ret = new T();
        ret.data = mixin("data "~op~" rhs.data");

        return ret;
    }
} 
```

D has chosen to use templates and allow mixin to handle operator overloading.

# Code Builder

I started working on improving a protocol buffer generator for D (fixing bugs and generating better API for D. In doing so I found the code snippets which generates the structure lacking. So I built Codebuilder. 

Why is it relevant to `mixin`? Because it works at compile-time (and runtime). This means the code builder results could be mixed in and compiled. I almost completed making the protobuf library use .proto files as "D code" (I think I left off on cross referencing. Also many of the unittests ran at compilation.)

{% github https://github.com/JesseKPhillips/codebuilder %} 

