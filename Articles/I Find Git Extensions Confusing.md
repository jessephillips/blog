I made the recommendation to us [git extensions](https://git-extensions-documentation.readthedocs.io/en/latest/git_extensions.html) as the standard git tool if you don't already have one. I thought this would be good, it sets up a diff tool and provides all the nice advanced git capabilities. So why do I end up so confused?

When I go to help I request an operation. They right click on something and the option is available. Well, that might do what I'm thinking, but I don't know. This creates a complex matrix of options to achieve something. 

It has some nice usability improvements, but I think it does not help to guide the user. You already need to understand what git is doing in order to make the most of it. 