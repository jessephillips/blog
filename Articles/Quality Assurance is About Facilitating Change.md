There are some really great statements about the purpose of testing. Is it to find issues? Are just communicating risk or function to stakeholders? Do we ensure requirements are met? Is it to advocate for the user.

Well, I think they might be all wrong, kidding those are levels below. At the heart quality assurance is working to allow changes to occur.

Regression testing is the clearest activity to support changes. If the software is modified, a regression pass is to identify unexpected changes. 

When verifying requirements it is in direct support of getting a change through as designed. 

Advocating for a user is about identifying needed changes which make the software better for a user.

---

Being in a QA role as an SDET, I'm building tools to improve the developers ability to make changes. Being focused at the contract level I am supporting larger changes then unittesting.

I think this mindset is important because it appears the other mindset is to stop change. Change increases risk. Change creates unknowns. Change means more work for testing. Thus we must only change the bear minimum. QA then advocate for avoiding change.

Don't update the third party libraries you use, that introduces unnecessary risk. That security patch, throw it away. The OS updates, ack... We should definitely stay on Java 7, who knows what 8 will bring.

Being an advocate for change means, I want you to refactor code. I want you to update to the latest tools and techniques. I know how valuable that can be for quality. 