The USA Constitution is not modified directly and instead modification is described in an amendment.

I played with the idea of using git to preserve the original and still edit the document. It hit hacker news recently so I went to work fixing some glaring problems, and I used gitmoji for some art. 

The biggest failure was I couldn't have it authored on the year ratified. Someone did something similar and played a trick the tools don't support, I didn't see it worth it.

Now there were a few comedic issues open and of course political issues. I've chosen to label and close, mainly because there is nothing I can do.

Now there were some good ideas I don't intend to follow through with. Namely the draft work on the Bill of Rights. I do like the idea of making unratified ones as pull requests. One of the challenges is the tools aren't meant for this type of collaboration as someone could contribute a pull request, but then the amendment lives outside this repo, but realistically that is probably fine.

But it also goes to show that advertising your project is important. 2 years and almost 2000 stars in 3 days. 