I feel like I get myself stuck in a circular argument in explaining the value of rebase interactive. It goes something like

You'll want to squash, reword, split these apart. Why? To get clearer messages about what is changing. Why? Because it helps you track your changes and makes it easier to rebase. 

It is not so much this is the argument I articulate. But as I help people work through a rebase I notice a pattern that people don't really know what is in their commits. Which makes sense, it is a lot of work for me to put changes together with intent.

When performing a rebase, commits which aren't targeted create a hectic conflict resolution. Many times changes are an experiment to see an outcome from a hypothesis. This is all important stuff and may need referenced in the experimentation. But when all is said and done it can be hard to see what happened and why.

The source code is in a working state, why spend so much time optimizing how to get there? Why communicate through commits when code review provides review of the end result and I can explain why we want this end state?

Personally I understand my changes better. When I try a first experiment often the commit message I wrote is wrong and misleading. If I am rebasing then a conflict on these commits could result in unnecessary work on something I didn't understand at the time and don't understand now.

Before I end it is important to note that understanding a change in this context does not mean understand why the changes worked. 