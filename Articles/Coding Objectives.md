There is much written on how to write good code and good unittests. Generally in normal production code you follow DRY, in test ARY (always repeat yourself). Well integration test framework fits in the middle of these.

You want to build out tests which are readable and you what to write code that supports repeating the important things about the test, but hides some of the more safisticated setup.

At this level of testing there is a more complete system you are working with, being able to improve the inner workings across all the tests can be important. It can help provide how to not only interact with the system but also details about the system. Log data access, sure you can analyze the logs after running tests to find issues, but having it associated with the steps to reproduce is important. 

Code standards need to adjust to meet the need, not to meet the standards. 