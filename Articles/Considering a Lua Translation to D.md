{% github JesseKPhillips/lua no-readme %}

I want to play with the idea of converting Lua from C to D. The thought here is that [LuaD](https://code.dlang.org/packages/luad) is a nice wrapper to Lua. I kind of wonder if Lua being written in D would open new doors of integration, compile time Lua?

I don't actually expect to make it very far in this project. I will likely not get much further than having D as part of the compilation. 

# Old School Development

It is a little weird coming at these projects. Best I can tell the official source code is only available through zip packages on the website. 

Even though the code is open source, development is not in the open. I found a repository on github.com which provides updates through version control. 

# Being More Modern

Ultimately I want the transition to be completed incrementally, so that throughout the whole process there is a working build of Lua.

To help with this I added github actions. In this case I don't have a good conceptual understanding of what is happening, likely hindered by my knowledge with gitlab pipeline.

* it appears only the first job has access to the source code (automatically) 
* yaml steps seem overly hierarchical

It appears actions are very powerful, but the yaml format is not specified (each action gets its own properties).

I had originally tried to use a different job for testing than I did for building. It did not end well. I think the online action editor is going to be critical to navigating actions. 

But I now have a CI system running tests which I can use as part of my conversion. 