---
title: Value in Dedicated Testing
published: true
description: 
tags: testing
---

What is software testing? What does a software tester do? What benefits do they bring to the table? James Bach has been trying save software testing by defining it a craft. But when you're hired to do software testing, you may not be tasked with the craft defined. So I want to put out there what I think I've brought to the table for the quality improvements irrespective of its classification.

I think the number one benefit I've brought into a team has been the investigative skills to make it possible to reproduce an issue and drill down into the cause of the issue. This means an issue has already been found and identified, many times when I've come into the team there is no infrastructure to handle observing and manipulating the system to understanding the behavior or to inject events known to be happening.

This type of value is very different from the expected role tasks. I'm expected to find the issue before any others see it. I'm expected to verify the developer met the requirements specified. I'm expected to check that all previous specified requirements continue to meet the defined behavior. All of these are things I participate in to some level. However I am looking at the changes and the system and taking my best guess effort to cover the critical areas at risk for the changes taking place. This means I spend my time eliminating entire classification and areas of tests in order to meet reasonable quality confidence in a reasonable time period.

I also participate in building out regression automation. The automation is helpful in calling out regression but I also believe certain focus I defined above can reduce the value of these regression tests. However, what I have recently determined to convince myself of some regression tests I consider low risk as still a major value in providing confidence. The focus here is that these low risk tests provide a coverage that eliminates classification of failure, such that when an issue does occur in the general area, focus can be directed quickly away from these low risk areas into areas less understood or covered.

The other aspect to the automated testing I end up writing is it provides the possibility to add the regression tests that cover many different types of situations. I'm still very diligent about where I focus my efforts to places known to be problems, but one of the challenges is convincing people that the symptoms which they see aren't always the problems which need the focus.

QA is not looking for what to test, but what not to test. There are so many things that constitute a test the search is actually being able to eliminate what can be left off for the release. This can come from risk, severity, code changes, and so much more.