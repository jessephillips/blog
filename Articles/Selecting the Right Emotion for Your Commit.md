---
title: Selecting the Right Emotion for Your Commit 
published: true
description: 
tags: git, gitmoji 
---

This idea that your emotions can tell a story is new to me, but it could be that emoji encompasses much more than emotions now.

I've been utilizing [gitmoji] (https://gitmoji.carloscuesta.me) for some time now. And many on the team are liking what it is doing to their commits, even if I push on some things which seem too hard to break up.

I wanted to explain a couple of these icons though. 

:sparkles: - New features
:recycle: - Refactoring 
:boom: - Breaking changes 
:building_construction: - Architecture changes

One of the rules is that you can't commit with more than one. And since the commit is reviewed to identify that is the only thing you're doing.

What had me stumble for a bit was how I decided between an architecture or breaking changes and here is what I came up with:

:building_construction: - :recycle: :boom:
:boom: - :sparkles: :boom:

They are both breaking changes. One is when your refacing went too far and changed an important api and the other is when it happens while adding a new feature.

This makes me think that a good migration plan would entail

:sparkles: 
:recycle: 
And
:fire: - Remove code 

The idea being a new api is provided before the old one is removed. 