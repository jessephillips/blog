I spent this effort on a cross-reference in the meta data to the content file. This eliminates a required convention where file name of the meta and content is the same, which is a nice convention but I want to allow folders to be specified. We'll see how this evolves.

https://gitlab.com/jessephillips/devarticlator/compare/v0.0.3...v0.0.4

```dlang
in(buildPath(folder,articleContentFolder).exists, format("Create direcorty `buildPath(\"%s\",articleContentFolder)` before calling to save articles.", folder))
```

I will be enforcing articles being written in a sub directory of the output location. I have updated the [contract](https://dlang.org/spec/contracts.html#pre_post_contracts) to check the sub folder exists.

I have also modify the error message to provide a code sample. I've decided this sub directory is not configurable making the error message more valuable.

```dlang
out(path) {
    assert(path.articleSaveLocation != path.metaSaveLocation, "Can't save to same file");
    assert(path.articleSaveLocation.endsWith(path.articleMetaPath), "Meta Reference to Article File must match.");
    assert(!path.articleSaveLocation.find(folder).empty, "Save Locations should utilized specified folder");
    assert(!path.metaSaveLocation.find(folder).empty, "Save Locations should utilized specified folder");
}
```

Since I'm creating an intricate relationship between the three paths, I wanted to verify that relationship.

To create this validation I needed to pull out the logic to create the paths from the method writing the data.

-----

I also wired up the dev.to api pulling. 

```dlang
if(!exists(buildPath(savePath, articleContentFolder)))
    mkdirRecurse(buildPath(savePath, articleContentFolder));

meArticles(data)
  .structureArticles
  .saveArticles(savePath);
```

Since I choose to delegate folder creation, the first step is creating the needed directory structure. 

After that the rest of the components have been built and all I need is to sequence the calls.

D also provides a way to [delegate the output](https://dlang.org/phobos/std_range_primitives.html#isOutputRange). When I looked into this I remember I also want to start using iopipe. But ultimately I did not feel that the output range concept was going to hold its weight. `saveArticles` is already pretty disconnected and could easily be replaced. 

-----

In this case I did not start with the tests or contracts, these were added after manual confirmation. 

```dlang
unittest {
    ArticleMeta meta;
    meta.title = "Article Title";
    articlePaths("someLocation", meta);
}
```

The specific unittest does no validation itself. Instead it exists to exercise the contract. By placing the test on the contract allow validation during code execution along with the unittests identified. 

For those concerned with performance, contracts are removed if building release.