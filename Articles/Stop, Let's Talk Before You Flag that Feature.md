---
title: Stop, Let's Talk Before You Flag that Feature
published: true
description: I don't recommend going down the feature flag workflow. 
tags: featureflag, devops
---

[Feature flags] (https://itnext.io/feature-flags-be-truly-agile-820ff50294c) are making their rounds and I want to step in. 

When you read the linked article it has a section for the bad, not being able to use a feature toggle is not a con to list against it. You will also see it is great for QA because you can break up you QA into feature testers.

Let me give you the actual cost of feature flags.

* You're adding non-production logic to your code
* QA now has more software versions to test

I think the first is pretty clear, more code means more chance for bugs, modifying existing code to provide toggling which would not have needed modified otherwise increases change, change equates to increase risk.

QA does not need more variables to control for. There is is an [algorithm](https://dlang.org/phobos/std_algorithm_setops.html#.Cartesian Product) to help you calculate all your new test scenarios. Here is the best part, we aren't going to test them all. 

Feature flags can work and they can work well. But there are other software development practices to follow before it will even have a chance of not creating a huge mess. 

One of the touted QA benefits was a broken environment taking days to fix. To be clear that commit/merge needs reverted ASAP.

Feature flags increase risk, especially if used the way touted by this article. A/B testing on the other hand has the same issues but you were told about needing extra production monitoring. 