# Preamble 

First off, a shout out to this article for the inspiration to start this. 

{% link https://dev.to/maxime1992/manage-your-dev-to-blog-posts-from-a-git-repo-and-use-continuous-deployment-to-auto-publish-update-them-143j %}

The first post should actually goto this other post I made.

{% link https://dev.to/jessekphillips/engaging-in-projects-40j4 %}

# The Objective 

I want to create a gitlab pipeline which will publish written articles to Dev.to.

The publication will be done utilizing D in Linux docker containers. How to utilize the project is yet to be determined. 

# The Timeline 

There isn't one. 

# This Series

I have decided to create a blog series to discuss the development efforts. It isn't about doing it "right" but maybe I'll be able to write about where I fell short and why.

# The Repo

https://gitlab.com/jessephillips/devarticlator

I have chosen to utilize gitlab for this project. I have been using gitlab at work and become familiar with the CI tools and the docker integration.

# The Content 

https://gitlab.com/jessephillips/devarticlator/tree/v0.0.1

At this juncture I have code which will pull me articles using a key provided as an environment variable. 

I have chosen the Vibe.d http library as it provides a good selection of tools and quite popular. 

What you will not see is the first attempts where the key was just pasted in.

Refamiliarizing myself with the Vibe.d Json api was the biggest effort.

## TDD

One thing to note is that I have no tests. TDD folks are screaming at me. But here is a confession, I don't know what I am doing. I don't know what interface I want exposed, and I don't know what a good interface would be. I see two ways out of this.

After exploring the api and what I'm doing I could throw out all the code. This would allow me to take what I learned and solidify a test for what I want. 

The other is to affirm that is the point of TDD, I need to create a design upfront and know what I'm trying to do. 