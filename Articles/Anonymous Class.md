An anonymous class is a way to provide a structured type which has no name making it optimal for single use, similar to delegates/lambda. 

I have been making use of this in C# to build data I serialize to Json. 

```dlang
// D
auto air = new class {
    auto thin = "I am real.";
};

pragma(msg, typeof(air));
// __anonclass1
```
D shows us the premise, we can assign an instance to a variable and the compiler builds an internal reference to the class. 

```csharp
// C#
var air = new {
    thin = "I am real"
};

Console.WriteLine(air.GetType());
// <>__AnonType0`1[System.String]
```
C# drops the need to specify class and field type but functions basically the same. It does print additional info about the field types. 

```golang
// Go
import "reflect"
air := struct {
	thin 	string
}{
	"I am real.",
}

fmt.Println(reflect.TypeOf(air))
// struct { thin string }
```
Go does not have classes but structures will serve the same purpose. In this case a field type is specified like D and its value is passed through constructor. The type print out looks just like the declaration. 

D has struct but they cannot be created anonymously. This is likely because D added anonymous class to make porting Java code easier (DWT is pretty cool).

```javascript
// Javascript 
const air = {
    thin: 'I am real.',
};
console.log(typeof air);
// object
```

With Javascript I did not use an anonymous class because I looked it up and found [it is not a good idea?](https://stackoverflow.com/questions/38739499/anonymous-class-instance-is-it-a-bad-idea). I wasn't sure what I would find for a dynamic language.

Lua will get an honorable mention. It does not have classes. Meta tables allow for some interesting things, but like Javascript anonymous is basically default. 

# Additional Thoughts 

In D if we repeat the same anonymous class structure the internal type is not reused. 

```dlang
// D
auto air2 = new class {
    auto thin = "I am real.";
};

pragma(msg, typeof(air3));
// __anonclass2
```

If we use meta programing you can create a new object of the same type. 

```dlang
// D
auto air3 = new typeof(air);
air3.thin = "still exist";

pragma(msg, typeof(air3));
// __anonclass1
```