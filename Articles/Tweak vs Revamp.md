---
title: Tweak vs Revamp
published: true
description: Taking development practices to the 23rd century. 
tags: discussion, leadership 
---

Here is a hard one, with established software solutions and release processes, how do you fit in a Revamp? 

Docker is taking front stage and with it management software is coming in to complete for mindshare. Developers build many products to help solve a development problem (dev.to advertises some).

Many of these solve real problems and could be beneficial to place into existing and future development but it could be a large undertaking. For my personal projects I likely would avoid buying anything, but if I'm making recommendations to the organization it is important to consider all appropriate options. 

Since I don't reach for these in personal projects, I don't want to waste a trial period on solving a problem and not have approval then loose the solution because I'm not planning my needs far enough ahead of time for finance.

I'm making good choices, but sometimes I wonder if there is an evaluation which should be done, which I don't have time for, at least not yet. 