---
title: Tweak Advice to Match Your Situation 
published: true 
description: 
tags: 
series: Humility
---

I try to post recommendations that I believe in and believe will be beneficial to others.

However I don't work in every situation. I also know there are always exceptions even well known situations. 

I'm wanting people to think about the situation they are in and how different views may help to improve their work even if the specifics don't fully work for them, and help me to remember to do the same. 

I expect I'll make similar posts to keep this humility in the minds of readers. 