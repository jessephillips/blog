Here I ended up creating an article generator for unittesting. I did start the work which prompted this need.

https://gitlab.com/jessephillips/devarticlator/compare/v0.0.5...v0.0.6

Building an article generator starts with how to build an single article. I have chosen to start with a json string which has some data placed in it. But having the exact same data when testing isn't advisable, though controlling what is different is also important.

https://gitlab.com/jessephillips/devarticlator/blob/v0.0.6/source/unittesting/articlegenerator.d#L51

In order to build out a list of articles, this single function can be utilized with the range generate template.

```dlang 
generate!fakeArticleMe;
```

This uses `generate` instead `repeat` because it will make a new object instead of repeating the same reference. But it does not change the article content. 

-------

https://gitlab.com/jessephillips/devarticlator/blob/v0.0.6/source/unittesting/articlegenerator.d#L58

I decided that I wanted to make modifications over the range. `sequence` will modify each article to create a descending sequence of articles. This is to simulate what the api would return.

```dlang 
generate!fakeArticleMe.seqence();
```

What I find interesting is that I can place this in a helping function and I would call that function 'generateFakeArticleMeSequence' true I might not utilize the term 'sequence' but the rest is accurate.

It appears I forgot to fill in the doc comment.

-----

I've never really figured out a good organization for imports. I that standard library, third party, local organization makes sense. 

https://gitlab.com/jessephillips/devarticlator/blob/v0.0.6/source/unittesting/articlegenerator.d#L3

-----

While I didn't implement this functionality, I do have an initial test to show the concept. 

https://gitlab.com/jessephillips/devarticlator/blob/v0.0.6/source/devtopull.d#L30

```dlang
auto devreq = generate!fakeArticleMe
    .seqence(ps.datePulled)
    .map!(x => x.deserializeJson!(ArticleMe))
    .take(3);
assert(devreq.filter!(x => x.isNewerArticle(ps)).empty);
```

I am looking to design the short circuit to use general range methods. And as I write this I realize that filter is the wrong choice as it will walk all articles. Instead I should have used `until`.

```dlang 
assert(devreq.until!(x => !x.isNewerArticle(ps)).empty);
```

The list would come out the same but this will rely on order of items to stop.

------

I think I wrote tests first, but I think I was adjusting the test as I developed. 