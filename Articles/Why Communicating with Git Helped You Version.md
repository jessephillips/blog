---
title: Why Communicating with Git Helped You Version
published: true
description: 
tags: git
canonical_url: https://he-the-great.livejournal.com/61227.html
series: Git Communication
---

In my previous postings about Git I talked about communication, and now that you've been communicating well we can get into what that means from a strategy standpoint.

Versioning is a large topic area and I do favor the idea of supporting several versions, but encourage limitations on that and discourage indefinite version support. But I want to focus on the idea of supporting the development versions: production, in flight, and new work.

New work is simple, you're preparing for the next release and you want to get everything you can into it.

The trouble comes when you're wanting to now make a change to production or an in flight release. If it is production the change should make it into all three, if it is in flight then it should go to new work also. Generally you will want to make the change to the oldest version first and cherry-pick and port into the later versions, but that is not always the case. Sometimes the bug you identified during new work becomes a desired fix in those older versions. Since you've been diligent about your communication you should now have a issue number and specific changes all ready to go and place into the older versions.

QA will be much happier if you're utilizing the original changes stored in your version history than recreating the changes through copy/paste or other manual means. You will also be happy if the changes are more complex than a few line changes.

I don't want to get into the actual [version designation](https://www.reddit.com/r/programming/comments/9qxtp9/tips_and_tricks_for_keeping_a_healthy_git/e8d2rc3/?utm_content=permalink), but the discussion on reddit is relevant.