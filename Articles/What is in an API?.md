I find that people assign a lot more requirements to being an application programming interface than exist, like having a REST endpoint or being public. Similar to how people expect more from [continuous integration](https://dev.to/jessekphillips/continuous-integration-with-rebase-2093).


To bring clarity in the purpose of the term 'API' I need to bring in another, ABI. You don't hear much about Application Binary Interface very often, how frequently do you program in binary?

The binary interface is something critical for compilers and virtual machines. In the C++ world the ABI is not a defined standard so compilers will define their own. This means libraries compiled in on compiler are no usable in another. D facilitates C++ integration by choosing to implement the ABI for the dominant compiler of the platform. For its own libraries D has not defined being ABI compatible across compiler releases, this means libraries need recompiled for each compiler version, potentially.

I hope that this helps to create some perspective on what is an API. In contrast to the ABI the Application Programming Interface is what the application programmer writes their code against. At the simplest level this is your function signatures.

The compiler has freedom in how it expects this signature is stored for calling. Is the left argument first on the stack or last?

-------

With this definition it should be clear, if you are using a text editor to write code you are making an API. Sense so much of the development community writing code works at this level, API in this form becomes useless.

We see articles talking about building APIs, they instruct how to build an API. What is happening? Well what they are asking is for you to consider the interface you're maintaining. Create stability in how your components talk to each other. It helps to isolate the work, if your micro service changes the rest API every release you have not decoupled implementation details with the integrated components. They would like to help you to define a good API. 