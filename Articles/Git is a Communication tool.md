---
title: Git is a Communication tool 
published: true 
description: Emphasize the power provided by git to communicate clearly 
tags: git
canonical_url: https://he-the-great.livejournal.com/60503.html
series: Git Communication
---

Historically there has been a number of complaints on the way git allows people to change history. There are continued discussions on using rebase, merge, and/or squash. I want to bring focus to the point of all these choices, communicating. I'm not looking at providing new suggestions, someone has already written about why you should do one thing or another.

Who might you want to be communicating with? 

* Future self
* Other Developers
* QA member
* Project Management

What is it you might want to communicate?

* That a feature is complete (to the best of your ability at this time)
* This code change is needed to fix bug X
* I have this important documentation update
* I was doing some work and my editor decided to change these files and I don't know why.
* I don't like the formatting of this document, here is my recommendation
* I completed X, Y, or Z and think it is important to get these upstream for everyone to benefit.
* Can we please upgrade our visual studio project files to the latest version of .net?

Now as a developer, wouldn't it be great if this is actually how you developed your work? Nice clear goals, good discipline in only doing one thing, ignoring that bug you found because it isn't the task at hand? Well, no it wouldn't be good you'll forget about that bug, or you will be so busy documenting that it needs fixed you forgot where you were in your task, it's just much better to get those changes into the code base and move on. And this is why Git's history rewrite is so valuable.

{% link https://dev.to/koffeinfrei/the-git-fixup-workflow-386d?utm_source=additional_box&utm_medium=internal&utm_campaign=regular&booster_org= %} 