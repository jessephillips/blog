---
title: Picking the Right Standards for Innovation
published: true
description: Consider the bounds of standards and allow for controlled exploration.
tags: discussion
series: standards
---

Standards are good, I don't ever want to come across as I don't believe in standards. But trouble can be had if the focus of standardizing targets the wrong things. It is standard practice to re-evaluate the standards every time they are using, including this standard.

[Do Standards Hinder Innovation](http://www.innovationmanagement.se/imtool-articles/do-standards-hinder-innovation/). I would say this falls into the everything category, everything in moderation... even moderation (as my wife likes to say). If you're choosing standards because they are standard, you're doing it wrong. Are you choosing them because you know standards exist for a reason... can you articulate that reason?

By definition standards are not innovation, if you have a core tenant that your company is an innovator... you best not be doing the standard, best practices. This doesn't mean standards shouldn't exist, it means you want people to break from the standards. You want those standards to be challenged. You don't want standards to be the way we've always done it.

[Rant] On my behalf the argument was made that the code had been around for years, because the concern was that it would be problematic as selenium changes. This didn't go over too well, "we've always done it that way" isn't a reasonable excuse. This creates a massive conflict, innovation, standards, not doing it the same as before, establishing repeatable practices but never do them again.