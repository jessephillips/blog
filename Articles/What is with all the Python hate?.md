I have reasons for preferring other languages over Python, but I'm actually not going to write about that.

Instead I am writing in response to this sentiment 

"They're not helpful to the people reading these articles[...] 

"If you really want D to succeed, simply evangelize it. Don't use it as a way to attack others." -- [@Safi](https://dev.to/safijari/comment/j12a)

# Articles in question

{% link https://dev.to/jessekphillips/list-comprehension-in-d-4hpi %}

{% link https://dev.to/jessekphillips/printing-in-d-4nec %}

# Response

This is a completely understandable position to take. To be clear I am not attacking the author, his programming ability, choice of design. I genuinely find these articles well written. So what is my intent when adding the comment? I would say it started here 

{% link https://dev.to/jessekphillips/boolean-in-d-943 %}

"I recently got to work outside my comfort zone, rather than scripting in D I was to utilize Python." 

This resulted in 14 posts of very basic objectives. As someone who has programmed for some time it is hard to realize the importance of articles on the basics.

Articles from other languages provide additional insight into what people find useful. This can be very inspirational to create similar content.

Now I link back to the original article to give credit where credit is due. I could leave it at that. However, I posted back to the original article to inform the original author.

This action is of course leaves the door open for language debate. Ultimately the article I'm writing is a direct comparison with a conclusion where I favor D, so yes, please debate my conclusions.

But what of my comment referencing this debatable topic? Well I did attempt to pull the discussion into my article. I probably could have done it earlier and be better about it.

Clearly article readers are there to learn something about Python. Maybe I should work on changing my messaging. 